export interface PhotoURL {
  M: string;
  thumbnail: string;
  XL: string;
}