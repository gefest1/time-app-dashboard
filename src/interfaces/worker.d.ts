import {PhotoURL} from './photo'

export interface Worker {
  __typename: string;
  _id: string;
  fullName: string;
  login: string;
  institution: {
    name: string;
  };
  photoURL: PhotoURL;
}