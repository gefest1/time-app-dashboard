export interface Service {
  _id: string;
  endTime: string;
  name: string;
  price: number;
  startTime: string;
}