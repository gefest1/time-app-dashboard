import {PhotoURL} from './photo'
import { AllowedInstitutionTypes } from './institutionType';
import { Worker } from './worker';
export interface Institution {
  _id: string;
  address: string;
  avatarURL: PhotoURL;
  averagePrice: number;
  city: string;
  dateAdded: string;
  description: string;
  email: string;
  galleryURLs: [PhotoURL];
  instagram: string
  latitude: number;
  longitude: number;
  name: string;
  phoneNumber: string;
  rating: number;
  ratingCount: number;
  tags: [string];
  type: AllowedInstitutionTypes;
  whatsApp: string
  workers: Worker
  numOfWorkers: number;
}