import React from 'react'
import { Typography, Card, Avatar, Stack } from '@mui/material';

import { Worker } from '../interfaces/worker';

interface WorkerProps {
  props: Worker;
};

const WorkerCard: React.FC<WorkerProps> = ({ props }: WorkerProps) => {
  const { fullName, institution } = props;

  return (
    <Card sx={{
      height: '120px',
      width: '360px',
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-evenly',
      borderRadius: '14px',
      boxShadow: 2,
    }}>
    <Avatar sx={{
      height: '72px',
      width: '72px',
    }}
      alt={`${fullName}-avatar`} />
      <Stack
        direction="column"
        spacing={2}
        sx={{
          mx: '12px',
        }}
        >
          <Typography sx={{
            fontWeight: '600',
            fontSize: '24px',
          }}>{fullName}</Typography>
          <Typography sx={{
            fontWeight: '300',
            fontSize: '16px',
        }}>{ institution.name}</Typography>
        </Stack>
      </Card>
  )
};

export default WorkerCard;
