import * as React from 'react';
<<<<<<< HEAD
=======
import Box from '@mui/material/Box';
import OutlinedInput from '@mui/material/OutlinedInput';
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
<<<<<<< HEAD
=======
import Chip from '@mui/material/Chip';
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};


interface SelectProps {
<<<<<<< HEAD
  items: any[];
  selectedItem: any;
  selectItems: (item: any) => void;
}

export default function MultipleSelectChip({items, selectedItem, selectItems}: SelectProps) {
=======
  items: string[];
  selectedItems: string[];
  selectItems: (item: []) => void;
}

export default function MultipleSelectChip({items, selectedItems, selectItems}: SelectProps) {
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73


  const handleChange = (event: { target: { value: any; }; }) => {
    const {
      target: { value },
    } = event;
<<<<<<< HEAD
    selectItems(value);
=======
    selectItems(
      typeof value === 'string' ? value.split(',') : value,
    );
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
  };

  return (
    <div>
<<<<<<< HEAD
      <FormControl
        sx={{
          m: 1,
          width: 300,
          borderRadius: '15px',
          boxShadow: 1,
        }}>
        <InputLabel>Везде</InputLabel>
        <Select
          value={selectedItem}
          onChange={handleChange}
          label="Везде"
          MenuProps={MenuProps}
          sx={{
            borderRadius: '15px',
          }}
=======
      <FormControl sx={{ m: 1, width: 300 }}>
        <InputLabel id="demo-multiple-chip-label">Chip</InputLabel>
        <Select
          labelId="demo-multiple-chip-label"
          id="demo-multiple-chip"
          multiple
          value={selectedItems}
          onChange={handleChange}
          input={<OutlinedInput id="select-multiple-chip" label="Chip" />}
          renderValue={(selected) => (
            <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}>
              {selected.map((value) => (
                <Chip key={value} label={value} />
              ))}
            </Box>
          )}
          MenuProps={MenuProps}
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
        >
          {items.map((item: string, index: number) => (
            <MenuItem
              key={index}
              value={item}
            >
              {item}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
}