import React from 'react';
import { Button } from '@mui/material';
import AddRoundedIcon from '@mui/icons-material/AddRounded';

interface Props {
  text: string;
  onClick: () => void;
}


const CustomButton = ({ text, onClick }: Props) => {
  return (
    <Button
      startIcon={<AddRoundedIcon/>}
      color="primary"
      variant="contained"
      size="large"
      sx={{
        mt: '8px',
        height: "32px",
        fontWeight: '600',
        fontSize: "16px",
        borderRadius: "14px",
      }}
      onClick={() => onClick()}
    >
      {text}
    </Button>
  )
};

export default CustomButton;