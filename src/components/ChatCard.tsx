import React from 'react'
import { Box, Stack, Typography, Avatar, Badge} from '@mui/material';
import NotificationsIcon from '@mui/icons-material/Notifications';
import dayjs from 'dayjs';
import { Chat } from '../interfaces/chat';
import { User } from '../interfaces/chat';

interface ChatCardProps {
  chat: Chat;
  setChat: () => void;
  user: User;
}


const ChatCard = ({ chat, setChat, user }: ChatCardProps) => {
  const date = dayjs(chat?.lastMessage?.dateSent).format('DD.MM.YYYY');
  return (
    <Box sx={{
      width: '100%',
      height: '76px',
      borderRadius: '12px',
      mt: '10px',
    }}
      onClick={setChat}
    >
      <Stack direction="row" justifyContent="space-between" alignItems="center" sx={{py: '10px', px: '20px'}}>
        {/* Main Section */}
        <Stack direction="row" alignItems="center" spacing={2}>
          <Avatar src={user?.photoURL?.XL} sx={{height: '64px', width: '64px'}}/>
          <div>
            <Typography variant="h4">{user?.fullName}</Typography>
            <Typography sx={{color: '#979797'}}>{chat?.lastMessage?.text}</Typography>
          </div>
        </Stack>
        {/* Time Secion */}
        <Stack direction="column" alignItems="center" spacing={3}> 
          <Typography sx={{color: '#979797'}}>{date}</Typography>
          {chat?.lastMessage?.isViewed && (
            <Badge badgeContent={1} color="error"/>
          )}
        </Stack>
      </Stack>
    </Box>
  )
};

export default ChatCard;