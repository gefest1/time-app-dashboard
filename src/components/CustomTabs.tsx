import * as React from 'react';
import {Box, Typography} from '@mui/material'

interface EmitValue {
  emitValue: (value: number) => void;
}

export default function CustomTab({emitValue}: EmitValue) {
  const [value, setValue] = React.useState(0);
  const handleChange = (newValue: number) => {
    setValue(newValue);
    emitValue(newValue);
  }
  
  return (
    <Box sx={{
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-evenly',
      alignItems: 'center',
      backgroundColor: '#EAEEF3',
      borderRadius: "16px",
      height: '46px',
    }}>
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          borderRadius: '16px',
          height: '80%',
          width: '46%',
          backgroundColor: value === 0 ? '#ffffff' : '',
          boxShadow: value === 0 ? 2 : '',
          color: value === 0 ? "" : "#007AFF",
        }}
        onClick={() => handleChange(0)}
      >
        <Typography variant="h6" >Активные</Typography>
      </Box>
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          borderRadius: '16px',
          height: '80%',
          width: '46%',
          backgroundColor: value === 1 ? '#ffffff' : '',
          boxShadow: value === 1 ? 2 : '',
          color: value === 1 ? "" : "#007AFF",
        }}
        onClick={() => handleChange(1)}
      >
        <Typography variant="h6">Завершенные</Typography>
      </Box>
    </Box>
  )
}
