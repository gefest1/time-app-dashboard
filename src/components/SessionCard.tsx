import React from 'react'
import dayjs from 'dayjs';
import { Card, Avatar, CardActions, CardContent, Typography, Button, Box } from '@mui/material'

import { Session } from '../interfaces/session'


interface Props {
  session: Session;
  setOpen: () => void;
}

const SessionCard = ({ session, setOpen } : Props) => {
  const { client, institution, startTime, endTime, price, status } = session;
  return (
    <Card sx={{
      maxWidth: "100%",
      minWidth: "75%",
      minHeight: '110px',
      maxHeight: "120px",
      display: "flex",
      flexDirection: 'row',
      justifyContent: "space-between",
      alignItems: 'center',
      my: "16px",
      px: '8px',
      borderRadius: "16px",
      boxShadow: 1,
    }}
      onClick = {() =>  setOpen()}
    >
      <CardContent sx={{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
      }}>
        <Avatar sx={{
          height: '60px',
          width: '60px',
          backgroundColor: status === 'Ongoing' ? '#00E440' : '#E40000',
          mx: '8px',
        }} />
        <Box sx={{
          display: 'flex',
          flexDirection: 'column',
        }}>
          <Typography variant="h5" sx={{
            fontWeight: 'bold'
          }}>
            {client?.fullName}
          </Typography>
          <Typography sx={{
            fontSize: "16px",
            fontWeight: "200"
          }}>
            {client?.phoneNumber}
          </Typography>
        </Box>
      </CardContent>
      <CardContent sx={{
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
      }}>
        {/* Club Thing */}
        <Typography sx={{
          fontWeight: '300',
          fontSize: '16px',
          mx: '4px',
      }}>
          {institution?.name}
        </Typography>
        <Typography sx={{
          fontWeight: '300',
          fontSize: '16px',
          mx: '4px',
      }}>
          {institution?.address}
        </Typography>
      </CardContent>
      <CardContent sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      }}>
        <Typography sx={{
          fontWeight: '300',
          fontSize: '16px',
        }}>
          {dayjs(startTime).format('HH:MM DD/MM/YYYY')}
        </Typography>
        <Typography sx={{
          fontWeight: '500',
          fontSize: '24px',
        }}>
          {dayjs(endTime).diff(startTime, 'minutes')} Мин
        </Typography>
        <Typography sx={{
          fontWeight: '500',
          fontSize: '24px',
        }}>
          {price || 0} ₸ 
        </Typography>
      </CardContent>
    </Card>
  )
}

export default SessionCard
