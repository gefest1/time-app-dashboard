import React from 'react'
import { Box, Typography, Card, CardContent, CardMedia, Stack, Paper } from '@mui/material';
import StarBorderRoundedIcon from '@mui/icons-material/StarBorderRounded';
import ReviewsOutlinedIcon from '@mui/icons-material/ReviewsOutlined';
import { Institution } from '../interfaces/institution';

interface ClubProps {
  props: Institution;
}

const ClubCard: React.FC <ClubProps> = ({ props }: ClubProps) => {
  const { _id,  address, avatarURL, averagePrice, name, rating, numOfWorkers, ratingCount } = props;
  return (
    <Box>
      <Card sx={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center',
        width: '480px',
<<<<<<< HEAD
        height: '540px',
=======
        height: '520px',
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
        boxShadow: 2,
        borderRadius: '16px',
        p: '12px',
      }}>
        <CardMedia
          sx={{
            m: '8px',
            borderRadius: '12px',
            width: "94%",
          }}
          component="img"
          height="320"
          image={avatarURL.XL}
          alt={name + '-image'}
        />
        <CardContent sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'flex-start',
          justifyContent: 'flex-start',
        }}>
          <Typography variant="h4" sx={{
            m: '16px',
            textTransform: 'capitalize',
          }}>
            {name}
          </Typography>
          {/* Info */}
          <Stack
            direction="row"
            justifyContent="space-between"
            alignItems="center">
            <Box sx={{
              display: 'flex',
              flexDirection: 'column',
              mx: "16px",
            }}>
              <Typography sx={{
                fontWeight: '200',
                fontSize: '16px',
              }}>
                {address}
              </Typography>
              <Typography sx={{
                fontWeight: '200',
                fontSize: '16px',
              }}>
                {`Команда: ${numOfWorkers} человек`}
              </Typography>
            </Box>
<<<<<<< HEAD
            <Stack
              direction="column"
              alignItems="center"
              justifyContent="center"
              spacing={2}
              sx={{
=======
            <Box sx={{
              display: 'flex',
              flexDirection: 'column',              
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
              mx:"16px",
            }}>
              <Typography sx={{
                fontWeight: '200',
                fontSize: '16px',
              }}>
                Пн-Пт: 10:00 — 20:00
              </Typography>
              <Typography sx={{
                fontWeight: '200',
                fontSize: '16px',
              }}>
                Сб-Вс: 14:00 — 22:00
              </Typography>
<<<<<<< HEAD
            </Stack>
=======
            </Box>
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
          </Stack>
          {/* Info Cards  */}
          <Stack sx={{
            justifyContent: 'space-between',
            alignItems: 'center',
            mx: '12px',
            my: '16px',
          }}
            direction="row"
            spacing={4}
          >
            <Paper sx={{
              backgroundColor: '#434343',
              borderRadius: '8px',
              width: '120px',
              height: '36px',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',

            }}>
              <Typography sx={{
                color: '#fff',
                fontWeight: '400',
                fontSize: '18px',
              }}>
                {averagePrice} T/мин
              </Typography>
            </Paper >
            <Paper sx={{
              backgroundColor: '#F9F9F9',
              borderRadius: '8px',
              width: '120px',
              height: '36px',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',

            }}>
              <StarBorderRoundedIcon sx={{
                color: '#00DF31',
                fontWeight: '400',
                fontSize: '24px',
                mx: '1px',
              }} />
              <Typography sx={{
                color: '#00DF31',
                fontWeight: '400',
                fontSize: '18px',
                mx: '2px',
              }}>
                {Math.round(rating * 100) / 100 || '0'}
              </Typography>
            </Paper>
            <Paper sx={{
              backgroundColor: '#F9F9F9',
              borderRadius: '8px',
              width: '120px',
              height: '36px',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',

            }}>
              <ReviewsOutlinedIcon sx={{
                color: '#434343',
                fontWeight: '400',
                fontSize: '18px',
                mx: '2px',
              }}/>
              <Typography sx={{
                color: '#434343',
                fontWeight: '400',
                fontSize: '24px',
                mx: '1px',
              }}>{ratingCount || '0'}</Typography>
            </Paper>
          </Stack>

        </CardContent>
      </Card>
    </Box> 
  )
}

export default ClubCard;