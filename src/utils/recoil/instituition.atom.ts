import { atom } from 'recoil';
import { Institution } from '../../interfaces/institution';

export const institutionAtom = atom<Institution[]>({
  key: 'institution',
  default: [{
    _id: '',
    address: "",
    avatarURL: {
      M: "",
      XL: "",
      thumbnail: "",
    },
    averagePrice: 0,
    city: "",
    dateAdded: "",
    description: "",
    email: "",
    galleryURLs: [{
      M: "",
      XL: "",
      thumbnail: "",
    }],
    instagram: "",
    latitude: 0,
    longitude: 0,
    name: "",
    phoneNumber: "",
    rating: 0,
    ratingCount: 0,
    tags: [""],
    type: "ActiveLeisure" || "Fitness" || "Entertainment",
    whatsApp: "",
    workers: {
      __typename: '',
      _id: "",
      fullName: "",
      login: "",
      institution: {
        name: "",
      },
      photoURL: {
        M: '',
        XL: "",
        thumbnail: ""
      },
    },
    numOfWorkers: 0,
  }]
})