import { useRecoilState } from 'recoil';
import { ownerAtom } from './owner.atom'

import { Owner } from '../../interfaces/owner';

export const useOwner: () => [
  Owner,
  {
    setOwner: (owner: Owner) => void;
  }
] = () => {
  const [owner, setCurrentOwner] = useRecoilState(ownerAtom);

  const setOwner = (owner: Owner) => setCurrentOwner(owner);
  
  return [
    owner, {
      setOwner,
    }
  ]
}
