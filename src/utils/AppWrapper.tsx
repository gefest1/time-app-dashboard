import React from 'react';
import { initializeApollo } from "./wrap/apolloClient";
import { ApolloProvider } from "@apollo/client";
import { RecoilRoot } from "recoil";


interface Props {
  children: React.ReactElement
}

const AppWrapper: React.FC<Props> = ({children}: Props) => {
  const apolloClient = initializeApollo();
  return (
    <>
      <ApolloProvider client={apolloClient}>
        <RecoilRoot>{children}</RecoilRoot>
      </ApolloProvider>
    </>
  );
}

export default AppWrapper;