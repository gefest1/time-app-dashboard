import { ApolloClient, InMemoryCache, NormalizedCacheObject, split, HttpLink } from "@apollo/client";
import { WebSocketLink } from '@apollo/client/link/ws';
import { getMainDefinition } from '@apollo/client/utilities';

let apolloClient: ApolloClient<NormalizedCacheObject> | null = null;


// const wsLink = new WebSocketLink({
//   uri: 'ws://167.172.162.8:3001/graphql',
//   options: {
//     reconnect: true
//   }
// });
// const httpLink = new HttpLink({
//   uri: 'http://167.172.162.8:3001/graphql'
// });

// const splitLink = split(
//   ({ query }) => {
//     const definition = getMainDefinition(query);
//     return (
//       definition.kind === 'OperationDefinition' &&
//       definition.operation === 'subscription'
//     );
//   },
//   wsLink,
//   httpLink,
// );

const createApolloClient = new ApolloClient({
    ssrMode: typeof window === "undefined",
    uri: 'http://167.172.162.8:3001/graphql',
    cache: new InMemoryCache(),
});

export const initializeApollo = () => {
    // For SSG and SSR always create a new Apollo Client
    if (typeof window === "undefined") {
        return createApolloClient;
    }

    // Create the Apollo Client once in the client
    if (!apolloClient) {
        apolloClient = createApolloClient;
    }

    return apolloClient;
};
