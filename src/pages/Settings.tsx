import React from 'react'
import { Stack, Box, TextField, Typography, Button } from '@mui/material';
<<<<<<< HEAD
import { useMutation } from '@apollo/client';
import { emitCustomEvent } from 'react-custom-events';

import { EDIT_OWNER } from '../graphql/mutations/editOwner';
import { useOwner } from '../utils/recoil/owner.hook';
import useLocalStorage from '../hooks/useLocalStorage';

const SettingsPage = () => {
  const [owner, { setOwner }] = useOwner();
  const [store] = useLocalStorage();
  const [editOwner, { loading, error }] = useMutation(EDIT_OWNER, {
    context: {
      headers: {
        Authorization: store.getItem("JWT")
      }
    }
  });
  if (loading) emitCustomEvent('loading', loading);
  emitCustomEvent('loading', false);
  if (error) emitCustomEvent('req-error', error);

  const handleEditOwner = (event:  React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    const formData = {
      email: data.get('email')?.toString(),
      phone: data.get('phone')?.toString(),
      oldPassword: data.get('oldPassword')?.toString(),
      newPassword: data.get('newPassword')?.toString(),
    }
    editOwner({variables: {...formData}})
  }
=======

import { useOwner } from '../utils/recoil/owner.hook';

const SettingsPage = () => {
  const [owner, { setOwner }] = useOwner();
  console.log(owner);
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
  return (
    <Stack direction="column" spacing={4}>
      <Typography variant="h4">Настройки</Typography>
      <Stack
        direction="column"
        spacing={2}
        justifyContent="center"
        alignItems="center"
<<<<<<< HEAD
        component="form"
        onSubmit={handleEditOwner}
=======
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
        sx={{
          background: '#fff',
          height: '460px',
          width: '610px',
          boxShadow: 2,
          borderRadius: '15px',
          px: '20px'
        }}>
        <Typography variant="h6">Главное</Typography>
<<<<<<< HEAD
        <TextField sx={{ width: "100%", height: '64px', }} type="email" name="email" variant="filled" label="Электронная почта" />
        <TextField sx={{ width: "100%", height: '64px', }} type="phone" name="phone" variant="filled" label="Телефон" />
        <TextField sx={{ width: "100%", height: '64px', }} type="password" name="oldPassword" variant="filled" label="Старый пароль" />
        <TextField sx={{ width: "100%", height: '64px', }} type="password" name="newPassword"variant="filled" label="Новый пароль" />
=======
        <TextField sx={{ width: "100%", height: '64px', }} type="email" variant="filled" label="Электронная почта" />
        <TextField sx={{ width: "100%", height: '64px', }} type="phone" variant="filled" label="Телефон" />
        <TextField sx={{ width: "100%", height: '64px', }} type="password" variant="filled" label="Пароль" />
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
        <Button
          type="submit"
          fullWidth
          variant="contained"
          sx={{ mt: 3, mb: 2 }}
        >Созранить Изменения</Button>
      </Stack>
      <Stack
        direction="column"
        spacing={2}
        justifyContent="center"
        alignItems="center"
        sx={{
          background: '#fff',
          height: '460px',
          width: '610px',
          boxShadow: 2,
          borderRadius: '15px',
          px: '20px'
        }}>
        <Typography variant="h6">Компания</Typography>
        <TextField sx={{ width: "100%", height: '64px', }} type="text" variant="filled" label="БИН/ИИН" />
        <TextField sx={{ width: "100%", height: '64px', }} type="text" variant="filled" label="Название" />
        <TextField sx={{ width: "100%", height: '64px', }} type="text" variant="filled" label="Ответственный" />
        <Button
          type="submit"
          fullWidth
          variant="contained"
          sx={{ mt: 3, mb: 2 }}
        >Изменить данные о комании</Button>
      </Stack>
    </Stack>
  )
};

export default SettingsPage;