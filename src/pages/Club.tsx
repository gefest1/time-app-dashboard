import React from 'react'
import { Typography, Grid, Stack, CircularProgress } from '@mui/material';

import { GET_WORKERS } from '../graphql/query/getWorkers';
import { useQuery } from '@apollo/client';
import { emitCustomEvent } from 'react-custom-events';

import useLocalStorage from '../hooks/useLocalStorage';
import { useInstitution } from '../utils/recoil/instituition.hook';
import { Institution } from '../interfaces/institution';
import { Worker } from '../interfaces/worker';

import ClubCard from '../components/ClubCard';
import WorkerCard from '../components/WorkerCard';
import CustomButton from '../components/CustomButton';
import CreateClub from './ClubModal/CreateClub';
import CreateWorker from './ClubModal/CreateWorker';

const Club: React.FC = () => {
  const [store] = useLocalStorage();
<<<<<<< HEAD
  const [institutions, { setInstitution }] = useInstitution();
=======
  
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
  const [openAddClub, setOpenAddClub] = React.useState<boolean>(false);
  const [openAddWorker, setOpenAddWorker] = React.useState<boolean>(false);

  const handleOpenCloseClub = () => {
    setOpenAddClub(!openAddClub)
  }
  const handleOpenCloseWorker = () => {
    setOpenAddWorker(!openAddWorker)
  }

<<<<<<< HEAD
=======
  const { data: institutionData, loading: institutionLoading, error: institutionError } = useQuery(GET_INSTITUTION, {
    context: {
      headers: {
        Authorization: store.getItem("JWT"),
      }
    }
  });
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
  const { data: workerData, loading: workerLoading, error: workerError } = useQuery(GET_WORKERS, {
    context: {
      headers: {
        Authorization: store.getItem("JWT"),
      }
    }
  })
  if (workerLoading) emitCustomEvent('loading', workerLoading);
  emitCustomEvent('loading', false);
  if (workerError) emitCustomEvent('error', workerError);

<<<<<<< HEAD
  const workers: Worker[] = workerData?.getWorkersOfAnOwner

  if (openAddClub) return (<CreateClub setOpen={handleOpenCloseClub} />)
  if (openAddWorker) return (<CreateWorker setOpen={handleOpenCloseWorker} institutions={institutions} />)
  
  return (
    <Grid
      container
      spacing={6}
      sx={{
        px: '12px'
      }}>
=======
  const institutions: Institution[] = institutionData.getInstitutionsOfOwner;
  const workers: Worker[] = workerData.getWorkersOfAnOwner

  if (openAddClub) return (<CreateClub setOpen={handleOpenCloseClub} />)
  
  return (
      <Grid container spacing={6} sx={{px: '12px'}}>
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
        {/* Clubs Header */}
        <Grid item xs={12}>
          <Stack
          direction="row"
          justifyContent="space-between"
          sx={{px: '20px',}}
        >
            <Typography sx={{
              height: "32px",
              fontWeight: '600',
              fontSize: "32px",
            }}
            >Объекты</Typography>
            <CustomButton text="Добавить" onClick={handleOpenCloseClub} />
          </Stack>
        </Grid>
        {/* Clubs Container */}
      <Grid
        container
        item
        spacing={6}>
          {institutions?.map((institution: Institution, index: number) => (
            <Grid item xs={6}>
              <ClubCard
                key={index}
                props={institution} />
            </Grid>
          ))}
        </Grid>
        {/* Workers Header */}
        <Grid item xs={12}>
          <Stack
            direction="row"
            justifyContent="space-between"
            alignItems="center">
            <Typography sx={{
              height: "32px",
              fontWeight: '600',
              fontSize: "32px",
            }}
<<<<<<< HEAD
            >Работники</Typography>
=======
            >Workers</Typography>
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
            <CustomButton text="Добавить" onClick={handleOpenCloseWorker} />
          </Stack>
        </Grid>
        {/* Workers Container */}
        <Grid container item spacing={2}>
          {workers?.map((worker: Worker, index: number) => (
            <Grid item xs={4}>
              <WorkerCard props={worker} />
            </Grid>
          ))}
        {openAddWorker && (<CreateWorker setOpen={handleOpenCloseWorker} institutions={institutions} />)}
      </Grid>
    </Grid>
  );
};

export default Club
