<<<<<<< HEAD
import React from 'react';
import { emitCustomEvent } from 'react-custom-events';
import dayjs from 'dayjs';
=======
import React from 'react'
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
import { Box, Stack, Typography, Avatar, Button, TextField, CircularProgress } from '@mui/material';
import ArrowBackRoundedIcon from '@mui/icons-material/ArrowBackRounded';
import AttachFileIcon from '@mui/icons-material/AttachFile';

import { useQuery, useSubscription, useMutation} from '@apollo/client';

import { CHAT } from '../../graphql/subscription/chat';
import { GET_PREV_MESSAGES } from '../../graphql/query/getPrevMessages';
import { LEAVE_MESSAGE } from '../../graphql/mutations/leaveMessage';

import useLocalStorage from '../../hooks/useLocalStorage';


import { Chat, Message, User } from '../../interfaces/chat';


interface ChatPageProps {
  chat: Chat;
  close: () => void;
  user: User
}

const ChatWindow = ({ chat, close, user }: ChatPageProps) => {
  const [store] = useLocalStorage();
<<<<<<< HEAD
  const [text, setText] = React.useState('');
  const messages: Message[] = []
  const { data: dataMessages, loading } = useQuery(GET_PREV_MESSAGES,
=======
  const [messages, setMessages] = React.useState([]);
  const [text, setText] = React.useState('');
  const { error, loading } = useQuery(GET_PREV_MESSAGES,
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
    {
      context: {
        headers: {
          Authorization: store.getItem('JWT'),
        },
      },
      variables: {
        "chatId": chat?._id,
<<<<<<< HEAD
        "lastDate": "",
      },
      onError: (error) => {
        emitCustomEvent('error', error)
      },
    }
  );
  const {data: subData, loading: subLoading , error } = useSubscription(CHAT, {
=======
        "lastDate": '',
      },
      onCompleted: (data) => {
        setMessages(data.getPreviousMessages)
      },
      onError: (error) => {
        console.log(error)
      },
    }
  );
  const {data: subData, loading: subLoading , error: subError } = useSubscription(CHAT, {
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
    context: {
      headers: {
        Authorization: store.getItem('JWT'),
      },
    },
    variables: {
      "chatId": chat?._id,
    },
  });
<<<<<<< HEAD
  const [leaveMessage, { data: msgData, loading: msgLoad, error: msgError }] = useMutation(LEAVE_MESSAGE, {
=======
  const [leaveMessage, { loading: msgLoad, error: msgError }] = useMutation(LEAVE_MESSAGE, {
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
    context: {
      headers: {
        Authorization: store.getItem('JWT'),
      },
    },
    variables: {
      "chatId": chat?._id,
      "text": text,
    },
    onCompleted: (data) => {
      console.log(data)
      setText('')
    }
<<<<<<< HEAD
  });
  console.log(subData?.payload)
  React.useEffect(() => {
    messages.push(msgData?.leaveMessage);
  }, [msgData?.leaveMessage])
  if (error || msgError) emitCustomEvent('error', error || msgError);
=======
  })
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73

  if (loading || subLoading) return (
    <CircularProgress sx={{
    alignSelf: 'center',
    justifySelf: 'center',
    my: 'auto',
<<<<<<< HEAD
    }} />)
  messages.push(...dataMessages.getPreviousMessages)
=======
  }}/>
  )

  console.log("SubData", subData)

  // React.useEffect(() => {
  //   setMessages(subData.viewChat)
  // }, [subData])
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73

  return (
    <Stack direction="column" alignItems="center" spacing={2} sx={{ width: '100%', px: '20px', height: '800px'}}>
      {/* Header Section*/}
      <Stack direction="row"
        justifyContent="space-between"
        alignItems="center"
        spacing={4}
        sx={{
          width: '100%',
          pt: '20px',
          mx: '20px',
          height: '70px',
          boxShadow: 2,
        }}>
        <Button
          color="primary"
          variant="text"
          startIcon={<ArrowBackRoundedIcon />}
          onClick={() => close()}
        >Назад</Button>
        <Typography variant="h4">{user?.fullName}</Typography>
        <Avatar src={user?.photoURL?.XL} sx={{ width: '40px', height: '40px' }} />
      </Stack>
      {/* Messages View */}
      <Stack direction="column" sx={{ backgroundColor: '#f9f9f9', width: '100%', overflow: 'scroll', minHeight: '650px'}}>
        {/* messages Shit */}
<<<<<<< HEAD
        {messages.map((message: Message, index: number) => <MessageBox message={message}/>)}
=======
        {messages.map((message, index: number) => <MessageBox message={message}/>)}
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
      </Stack>
      {/* Send Messages Section */}
      <Stack
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        sx={{
          height: '80px',
          width: '100%',
        }}
      >
        <AttachFileIcon sx={{transform: 'rotate(45deg)',}}/>
        <TextField
          sx={{
            width: '80%',
          }}
          value={text}
          onChange={(event) => setText(event.target.value)} />
        <Button
          variant="contained"
          color="primary"
          onClick={() => leaveMessage()}>
          отправить</Button>
      </Stack>
    </Stack>
  )
};

export default ChatWindow;


interface MessageProps {
  message: Message;
}
const MessageBox = ({ message }: MessageProps) => {
  const M = () => {
    return (
      <Box sx={{
        my: '10px',
        mx: '10px',
        borderRadius: '15px',
        color: message?.user?.__typename !== 'Owner' ? '#979797' : '#fff',
        backgroundColor: message?.user?.__typename !== 'Owner' ? '#fff' : '#62B4FF',
        minWidth: '50%',
        minHeight: '60px',
        alignItems: 'center',
        justifyContent: 'center',
      }}>
        <Typography>
          {message?.text}
        </Typography>
      </Box>
    )
  }

  return message?.user?.__typename !== 'Owner' ? (
      <Stack direction="row" justifyContent="space-between" alignItems="center">
        <M/>
<<<<<<< HEAD
        <div>{ dayjs(message.dateSent).format('DD.MM.YY')}</div>
      </Stack>
  ) : (
    <Stack direction="row" justifyContent="space-between" alignItems="center">
      <div>{dayjs(message.dateSent).format('DD.MM.YY')}</div>
=======
        <div></div>
      </Stack>
  ) : (
    <Stack direction="row" justifyContent="space-between" alignItems="center">
      <div></div>
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
      <M />
    </Stack>
  )  
}