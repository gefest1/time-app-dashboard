import React from 'react';
import { Box, Button, Container, CssBaseline, Grid, TextField, Typography, Link, FormControlLabel, Checkbox } from '@mui/material';
import { useLazyQuery } from '@apollo/client';
import { Copyright } from '@mui/icons-material';
import { emitCustomEvent } from 'react-custom-events';

<<<<<<< HEAD
import { useOwner } from '../utils/recoil/owner.hook';
import { LOGIN } from '../graphql/query/login';
import useLocalStorage from '../hooks/useLocalStorage';
=======
import {useOwner} from '../utils/recoil/owner.hook'
import { LOGIN } from '../graphql/query/login'
import useLocalStorage from '../hooks/useLocalStorage'
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73

const Login: React.FC = () => {
  const [owner, { setOwner }] = useOwner()
  const [store] = useLocalStorage();
  
  const [loginQuery, {  called, loading, error }] = useLazyQuery(LOGIN, {
    onCompleted: (data) => {
<<<<<<< HEAD
      setOwner(data.loginAsOwner )
=======
      setOwner(data.loginAsOwner)
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
      store.setItem("JWT", data.loginAsOwner.token)
      window.location.reload();
    }
  })
  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    const data = new FormData(event.currentTarget);
    const formData = {
      login: data.get('email')?.toString() || "",
      password: data.get('password')?.toString() || "",
    }
    loginQuery({ variables: {...formData} })
  };

<<<<<<< HEAD
  if (called && loading) emitCustomEvent('login-loading', loading);
  emitCustomEvent('login-loading', false);
  if (error) emitCustomEvent('login-error', error);
  
=======
  if (called && loading) return (<h1>Loading...</h1>);
  if (error) return (<h1>{error.message}</h1>);
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
  return (
    <Container component="main" maxWidth="xs" sx={{mt: '240px'}}>
        <CssBaseline />
        <Box
        sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Typography component="h1" variant="h5">
            Вход
          </Typography>
          <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email адрес"
              name="email"
              autoComplete="email"
              autoFocus
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Пароль"
              type="password"
              id="password"
              autoComplete="current-password"
            />
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Запомнить"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Войти
            </Button>
            <Grid container>
              <Grid item xs>
                <Link href="#" variant="body2">
                  Забыли пароль?
                </Link>
              </Grid>
              <Grid item>
                <Link href="#" variant="body2">
                  {"Регистрация"}
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
        {/* <Copyright sx={{ mt: 8, mb: 4 }} /> */}
      </Container>
  )
}

export default Login
