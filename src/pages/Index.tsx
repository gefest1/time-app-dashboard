import * as React from 'react';
import { styled, useTheme } from '@mui/material/styles';
import { Box, Container, List,  ListItemButton, ListItemIcon, ListItemText, Stack, Typography, CircularProgress} from '@mui/material/';
import  HomeRoundedIcon from '@mui/icons-material/HomeRounded';
import DashboardRoundedIcon  from '@mui/icons-material/DashboardRounded';
import DateRangeRoundedIcon from '@mui/icons-material/DateRangeRounded';
// import ChatBubbleRoundedIcon from '@mui/icons-material/ChatBubbleRounded';
import SettingsRoundedIcon from '@mui/icons-material/SettingsRounded';
import LogoutRoundedIcon from '@mui/icons-material/LogoutRounded';
import { Copyright } from '@mui/icons-material'
import { useCustomEventListener } from 'react-custom-events';
import { useQuery } from '@apollo/client';

import { GET_INSTITUTION } from '../graphql/query/getInstitution';
import useTabSwitch from '../hooks/useTabSwitch';
import useLocalStorage from '../hooks/useLocalStorage';
import { useInstitution } from '../utils/recoil/instituition.hook';
import Main from './Main';
import Club from './Club';
import HistoryPage from './History';
import SettingsPage from './Settings'
// import ChatPage from './Chat';

const menuList = [
  {
    text: 'Главная',
    icon: (<HomeRoundedIcon />),
  },
  
  {
    text: 'Клубы',
    icon: (<DashboardRoundedIcon />),
  },
  {
    text: 'История',
    icon: (<DateRangeRoundedIcon />),
  },
  // {
  //   text: 'Чат',
  //   icon: (<ChatBubbleRoundedIcon />),
  // },
  {
    text: 'Настройки',
    icon: (<SettingsRoundedIcon />),
  }
]



export default function PersistentDrawerLeft() {
  const theme = useTheme();
  const [store] = useLocalStorage();
  const [institutions, { setInstitution }] = useInstitution();
  const [selectedIndex, setSelectedIndex] = React.useState(0);
  const [tab, changeTab] = useTabSwitch(DrawerContent)
  const [loading, setLoading] = React.useState<boolean>(false);
  const [error, setError] = React.useState<any>();
  const logout = () => {
    store.removeItem('JWT')
    window.location.reload();
  }

  const handleListItemClick = (
    index: number,
  ) => {
    setSelectedIndex(index);
    changeTab(index)
  };
  const { loading: institutionLoading, error: institutionError } = useQuery(GET_INSTITUTION, {
    context: {
      headers: {
        Authorization: store.getItem("JWT"),
      }
    },
    onCompleted: (data) => {
      setInstitution(data?.getInstitutionsOfOwner)
    }
  });
  const drawer = (
    <Box sx={{
      display: 'flex',
      flexDirection: 'column',
    }}>
      <Box sx={{
        backgroundColor: '#fff',
        display: 'flex',
        flexDirection: 'column',
        boxShadow: 3,
        borderRadius: '15px',
      }}>
          <List>
          {menuList.map((item, index: number) => (
            <ListItemButton key={index}
              selected={selectedIndex === index}
              onClick={() => handleListItemClick(index)}
            >
                <ListItemIcon>
                  {item.icon}
                </ListItemIcon>
                <ListItemText primary={item.text} />
                </ListItemButton>
              ))
            }
          </List>
      </Box>
      <List sx={{
        backgroundColor: '#fff',
        marginTop: '20px',
        boxShadow: 3,
        borderRadius: '15px',
      }}>
        <ListItemButton key="Выход" onClick={logout}>
          <ListItemIcon>
            <LogoutRoundedIcon/>
          </ListItemIcon>
          <ListItemText primary="Выход"/>
        </ListItemButton>
      </List>
    </Box>
  )
  useCustomEventListener('loading', (data: boolean) => {
    setLoading(data);
  });
  useCustomEventListener('error', (data) => {
    console.log(data);
    setError(data);
  })
  return (
    <Stack justifyContent="space-between" sx={{height: '100vh'}}>
      <Stack
          direction="row"
          justifyContent="center"
          spacing={4}
          sx={{
            mt: '40px',
            mx: '10%',
        }}>
          {drawer}
          <Container sx={{
            display: 'flex',
            flexDirection: 'column',
          }}>
          {loading ? (
            <CircularProgress sx={{
              alignSelf: 'center',
              justifySelf: 'center',
              my: 'auto',
            }} />) :
            tab}
        </Container>
      </Stack>
      <Stack
        direction="row"
        alignItems="center"
        justifyContent="space-between"
        sx={{
          mx: '10%',
          mt: '80px',
          bottom: '20px'
        }}
        >
          <Stack
            direction="column"
            alignItems="flex-start"
            justifyContent="flex-start"
          >
            <Typography sx={{
              fontWeight: '600',
              fontSize: '42px',
              color: '#0F84F4',
            }}>time</Typography>
            <Typography sx={{ fontWeight: '300', fontSize: '14px', color: '#979797'}}>
            <Copyright sx={{height: '14px', width: '14px'}}/>
              TimeApp, 2021
            </Typography>
        </Stack>
        <Stack
          direction="column"
          alignItems="flex-end"
          justifyContent="flex-end"
        >
          <Typography variant="h4" >
            +7 777 777 77 77
          </Typography>
          <Typography sx={{ fontWeight: '300', fontSize: '14px', color: '#979797'}}>
            Служба поддержки
          </Typography>
        </Stack>
      </Stack>
    </Stack>
  );
}

const DrawerContent = [
  {
    component: (<Main />)
  },
  {
    component: (<Club />)
  },
  {
    component: (<HistoryPage />)
  },
  // {
  //   component: (<ChatPage />)
  // },
  {
    component: (<SettingsPage />)
  },
]