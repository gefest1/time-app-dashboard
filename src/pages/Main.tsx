import React from 'react';
import { Grid, Box, Typography, Pagination } from '@mui/material';
import CustomSelect from '../components/CustomSelect';
import SessionCard from '../components/SessionCard';
import ClientPage from './MainModal/ClientPage'
import dayjs from 'dayjs';
import { emitCustomEvent } from 'react-custom-events';

import ClientPage from './MainModal/ClientPage'

import { useQuery } from '@apollo/client';
import { VISITATIONS_OF_CLIENT } from '../graphql/query/visitationsOfClient';
import {GET_MAX_PAGES} from '../graphql/query/getMaxPages';
import { useInstitution } from '../utils/recoil/instituition.hook';
import useLocalStorage from '../hooks/useLocalStorage';
import { Session } from '../interfaces/session';
import { Institution } from '../interfaces/institution';
import {Client} from '../interfaces/client';

const Main: React.FC = () => {
  const [sessionType, setSessionType] = React.useState<number>(0);
  const [store] = useLocalStorage();
  const [institutions, { setInstitution }] = useInstitution();
  const [openClient, setOpenClient] = React.useState<boolean>(false);
  const [client, setClient] = React.useState<Client>()
  const [selectedInstituition, setSelectedInstitution] = React.useState<Institution>();
  const [page, setPage] = React.useState(1);
  
  const handlePageChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value);
  };

  const {
    data,
    loading,
    error
  } = useQuery(VISITATIONS_OF_CLIENT, {
    context: {
      headers: {
        Authorization: store.getItem('JWT')
      }
    },
    variables: {
      "institutionID": "" || selectedInstituition?._id,
      "page": page,
      "sortBy": "Date",
      "isActive": sessionType === 0,
    }
  });

  const {
    data: maxPageData,
    loading: maxPageLoading,
    error: maxPageError,
  } = useQuery(GET_MAX_PAGES, {
    context: {
      headers: {
        Authorization: store?.getItem('JWT')
      }
    },
    variables: {
      "institutionIds": institutions?.map(institution => institution?._id),
      "currentPage": page
    }
  })


  if (loading || maxPageLoading) {
    emitCustomEvent('loading', loading || maxPageLoading)
  }
  emitCustomEvent('loading', false)
  if (error || maxPageError) emitCustomEvent('error', error || maxPageError)
  
  const sessions: Session[] = data?.getVisitationsForAdminPanel;
  const pages: number = maxPageData?.getMaximumPageOfVisitationsForOwner;
  
  const handleOpenClient = (index: number) => {
    setClient(sessions[index]?.client);
    setOpenClient(true);
  }
  if (openClient && client) return <ClientPage client={client} close={() => setOpenClient(false)} />

  return (
    <Grid container spacing={2} sx={{ position: 'fixed', width: '80%', height: '80vh'}}>
      {/* Header Section */}
      <Grid container sx={{my: '10px'}}>
        <Grid item xs={6}>
          <Typography
            variant="h4"
            noWrap
            component="div"
            sx={{
              color: 'black',
            }}>
            Сессии за сегодня - {dayjs(new Date()).format('DD.MM.YYYY')}
          </Typography>
        </Grid>
        <Grid item xs={6}>
          <CustomSelect
            items={institutions.map(institution => institution.name)}
            selectItems={(item: Institution) => setSelectedInstitution(item)}
            selectedItem={selectedInstituition?.name}
          />
        </Grid>
      </Grid>
      {/* Tabs & Search Section */}
      <Grid container sx={{my: "24px"}}>
        <Grid container item xs={10} spacing={2}>
          {/* TAB Section */}
          <Grid item xs={5}>
            <Box sx={{
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-evenly',
      alignItems: 'center',
      backgroundColor: '#EAEEF3',
      borderRadius: "16px",
      height: '46px',
    }}>
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          borderRadius: '16px',
          height: '80%',
          width: '46%',
          backgroundColor: sessionType === 0 ? '#ffffff' : '',
          boxShadow: sessionType === 0 ? 2 : '',
          color: sessionType === 0 ? "" : "#007AFF",
        }}
        onClick={() => setSessionType(0)}
      >
        <Typography variant="h6" >Активные</Typography>
      </Box>
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          borderRadius: '16px',
          height: '80%',
          width: '46%',
          backgroundColor: sessionType === 1 ? '#ffffff' : '',
          boxShadow: sessionType === 1 ? 2 : '',
          color: sessionType === 1 ? "" : "#007AFF",
        }}
        onClick={() => setSessionType(1)}
      >
        <Typography variant="h6">Завершенные</Typography>
      </Box>
    </Box>
    </Grid>
        </Grid>
        {/* Session Section */}
        <Grid item xs={8} md={8} sx={{mt: '16px', overflow: 'scroll', height: '660px'}}>
          {sessionType === 0 ? sessions?.filter((session: Session) =>
            session.status === "Ongoing"
                && (
            (selectedInstituition === undefined)
            ||
            (session.institution?.name === selectedInstituition)
          )
          )?.map((session: Session, index: number) => (
            <SessionCard key={index} session={session} setOpen={() => handleOpenClient(index)} />
            )
          ) :
            sessions?.filter((session: Session) => (
              session.status === "Payed" || "Unpayed"
            ) && (
              (selectedInstituition === undefined)
              ||
              (session.institution?.name === selectedInstituition)
            )
            )?.map((session: Session, index: number) => (
            <SessionCard key={index} session={session} setOpen={() => handleOpenClient(index)} />
            )
            )}
        </Grid>
        <Grid item xs={8} md={8} sx={{mb: '16px', }}>
          <Pagination count={pages} page={page} onChange={handlePageChange} sx={{mx: 'auto'}}/>
        </Grid>
      </Grid>
      {/* Today Earning Section*/}

    </Grid>
  );
}


export default Main;