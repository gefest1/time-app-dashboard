import React from 'react'
<<<<<<< HEAD
import { Stack, Button, Typography, Avatar, Divider, Pagination } from '@mui/material';
import ArrowBackRoundedIcon from '@mui/icons-material/ArrowBackRounded';
import { emitCustomEvent } from 'react-custom-events';
import { useQuery, useMutation } from '@apollo/client';
import dayjs from 'dayjs';

import { END_SESSION } from '../../graphql/mutations/endSession';
import { GET_CLIENT_BY_ID } from '../../graphql/query/getClientByID';
import { GET_STATS_FOR_CLIENT } from '../../graphql/query/getStatsForClient';
import useLocalStorage from '../../hooks/useLocalStorage';
import { useInstitution } from '../../utils/recoil/instituition.hook';
=======
import { Stack, Button, IconButton, Typography, Avatar } from '@mui/material';
import ArrowBackRoundedIcon from '@mui/icons-material/ArrowBackRounded';

import { useQuery, useMutation } from '@apollo/client';
import { END_SESSION } from '../../graphql/mutations/endSession';
import { GET_CLIENT_BY_ID } from '../../graphql/query/getClientByID';

import useLocalStorage from '../../hooks/useLocalStorage';
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73

import { ReactComponent as AvPayClient } from '../../images/av_pay_client.svg';
import { ReactComponent as AvTimeClient } from '../../images/av_time_client.svg';

import { Client } from '../../interfaces/client';
import { Session } from '../../interfaces/session';
<<<<<<< HEAD
import { Institution } from '../../interfaces/institution';

interface Props {
  client: Client;
=======

interface Props {
  client?: Client;
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
  close: () => void;
}

const ClientPage = ({ client, close }: Props) => {
  const [store] = useLocalStorage();
<<<<<<< HEAD
  const [ , { setInstitution }] = useInstitution();
  const [sessions, setSessions] = React.useState<Session[]>()
  const [selectedInstituition, setSelectedInstitution] = React.useState<Institution>();
  const [page, setPage] = React.useState<number>(1);
  
  const handlePageChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value);
  };
  const { loading, error } = useQuery(GET_CLIENT_BY_ID, {
    context: {
      headers: {
        'Authorization': store?.getItem('JWT'),
=======
  const [session, setSession] = React.useState<Session[]>()
  const { loading, error } = useQuery(GET_CLIENT_BY_ID, {
    context: {
      headers: {
        'Authorization': store.getItem('JWT'),
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
      }
    },
    variables: {
      "clientId": client?._id,
<<<<<<< HEAD
      "page": page,
      "sortBy": "Date",
    },
    onCompleted: (data) => {
      setSessions(data?.getVisitationsOfAClientForAdminPanel)
    }
  })
  const {
    data: statsData,
    loading: statsLoading,
    error: statsError,
  } = useQuery(GET_STATS_FOR_CLIENT, {
    context: {
      headers: {
        'Authorization': store?.getItem('JWT'),
      },
    },
    variables: {
      "clientId": client?._id,
    },
  })
  const [endSessions, {
    loading: endLoading,
    error: endError
  }] = useMutation(END_SESSION, {
    context: {
      headers: {
        'Authorization': store?.getItem('JWT'),
=======
      "isActive": true,
      "page": 1,
      "sortBy": "Date",
    },
    onCompleted: (data) => {
      setSession(data.getVisitationsOfAClientForAdminPanel)
    }
  })
  const [endSession, { loading: endLoading, error: endError }] = useMutation(END_SESSION, {
    context: {
      headers: {
        'Authorization': store.getItem('JWT'),
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
      }
    },
    onCompleted: (data) => {
      console.log(data)
    }
<<<<<<< HEAD
  });

  const handleSessionEnd = (
    clientId: string = "",
    sessionsId: string = ""
  ) => {
    endSessions({
      variables: {
        "clientId": clientId,
        "sessionsId": sessionsId,
      }
    })
  };

  if (loading || endLoading || statsLoading) emitCustomEvent('loading', loading || endLoading || statsLoading);
  emitCustomEvent('loading', false);
  if (error || statsError) emitCustomEvent('error', error || statsError);
  if (endError) emitCustomEvent('endError', endError);

  if (sessions === undefined) return (<></>)
  const activeSessions = sessions?.filter((session: Session) => session?.status === "Ongoing");

  return (
    <Stack
      direction="column"
      alignItems="start"
      justifyContent="center"
      spacing={2}
    >
=======
  })
  const handleSessioEnd = (clientId: string = "", sessionId: string = "") => {
    endSession({
      variables: {
        "clientId": clientId,
        "sessionId": sessionId,
    }})
  }
  if (error) return (<h1>{error.message} </h1>);
  if (loading) return (<h1>loading </h1>);
  if(session === undefined) return (<></>)
  return (
    <Stack direction="column" alignItems="start" justifyContent="center">
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
      <Button
        color="primary"
        variant="text"
        startIcon={<ArrowBackRoundedIcon />}
        onClick={() => close()}
      >Назад</Button>
      {/* Header Section */}
<<<<<<< HEAD
      <Stack
        direction="row"
        spacing={6}
        justifyContent="center"
        alignItems="center"
        sx={{
          height: '240px',
          my: '24px'
        }}>
        {/* Client Info Paper */}
        <Stack
          direction="row"
          spacing={6}
          justifyContent="center"
          alignItems="center"
          sx={{
            width: '500px',
            boxShadow: 2,
            borderRadius: '12px',
            px: '20px',
            py: '20px',
            height: '100%'
          }}>
          <Avatar
            src={client?.photoURL?.XL}
            sx={{
              width: '140px',
              height: '140px'
            }} />
          <Stack
            direction="column"
            spacing={2}
            justifyContent="center">
            <Typography
              variant="h3" >
              {client?.fullName}
            </Typography>
            {
              client?.sex && client?.dateOfBirth ?
              (
                <Typography>{client?.sex}, {client?.dateOfBirth}</Typography>
              ) : (
                <></>
              )
            }
            <Typography sx={{
              fontWeight: '300',
              fontSize: '14px',
              color: '#979797',
            }}>
              Номер телефона
            </Typography>
            <Typography
              sx={{
                fontWeight: '400',
                fontSize: '18px',
                color: '#007AFF',
              }}>
              {client?.phoneNumber}
            </Typography>
            {client?.email ? (
              <>
                <Typography
                  sx={{
                  fontWeight: '300',
                  fontSize: '14px',
                  color: '#979797',
                  }}>
                  Email
                </Typography>
                <Typography
                  sx={{
                    fontWeight: '400',
                    fontSize: '18px',
                    color: '#007AFF',
                  }}>
                  {client?.email}
                </Typography>  
              </>
              ): (
                  <></>
              )
            }
          </Stack>
        </Stack>
        {/* Money Info */}
        <Stack
          direction="column"
          spacing={2}
          justifyContent="center"
          sx={{
            height: '100%',
          }}>
          {/* Average Price / Duration */}
          <Stack
            direction="row"
            spacing={2}>
            {/* Average Price per Visit */}
            <Stack
              direction="row"
              alignItems="center"
              justifyContent="start"
              spacing={4}
              sx={{
                py: '20px',
                px: '20px',
                boxShadow: 2,
                borderRadius: '12px',
                backgroundColor: '#F8F8F8'
              }}
              >
              <AvPayClient /> 
              <Stack
                direction="column">
                <Typography
                  variant="h4">
                  {statsData?.getPersonalStatsByClientIdForOwner?.averageMoneySpent} ₸
                </Typography>
                <Typography>
                  Средняя цена за одно посещение
                </Typography>
              </Stack>
            </Stack>
              {/* Average Time per Visitation */}
            <Stack
              direction="row"
              alignItems="center"
              justifyContent="start"
              spacing={4}
              sx={{
                py: '20px',
                px: '20px',
                boxShadow: 2,
                borderRadius: '12px',
                backgroundColor: '#F8F8F8'
              }}>
              <AvTimeClient />
              <Stack
                direction="column"
              >
                <Typography
                  variant="h4">
                  {statsData?.getPersonalStatsByClientIdForOwner?.averageTimeSpent} мин
                </Typography>
                <Typography>
                  Среднее время одного посещения
                </Typography>
                </Stack>
            </Stack>
              {/* Overall Earnings of all Visitations */}
          </Stack>
          <Stack
            direction="row"
            alignItems="center"
            justifyContent="start"
            spacing={4}
            sx={{
              py: '4px',
              px: '12px',
              boxShadow: 2,
              borderRadius: '12p',
              backgroundColor: '#F8F8F8',
              height: '140px'
            }}>
            <AvTimeClient />
            <Stack
              direction="column">
              <Typography
                variant="h4">
                {statsData?.getPersonalStatsByClientIdForOwner?.moneySpent} ₸
              </Typography>
              <Typography>
                Общая сумма всех посещений
              </Typography>  
            </Stack>
          </Stack>
        </Stack>
      </Stack>
      {/* Active Sessions - Secion */}
      {activeSessions.length ? (
      <Stack
        direction="column"
        spacing={4}
        justifyContent="flex-start"
        alignItems="center"
        sx={{
          backgroundColor: '#F8F8F8',
          borderRadius: '12px',
          my: '24px',
          py: '12px',
          width: '100%',
        }}>
        <Typography
          variant="h4"
        >
          Активные сессии
        </Typography>
        <ClientSessionCard
          session={activeSessions[0]}
          active={true}
          endSession={
            () => handleSessionEnd(
              client?._id,
              activeSessions[0]?._id)
          }/>
      </Stack>
      ) : (
          <></>
      )}
      
      {/* Histofy Of Sessions - Section */}
        {/* History Header - Section */}
      <Stack
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        sx={{
          mt: '10px',
        }}>
        <Typography
          variant="h4"
          sx={{
            color: '#434343',
            my: '20px',
          }}>
          История Посещений
        </Typography>
        {/* Switch Dropdown */}
      </Stack>
      <Divider />
        {/* History List - Section */}
       <Stack
        direction="column"
        spacing={2}
        sx={{
          overflow: 'scroll',
          maxHeight: '600px',
          width: '100%',
          boxShadow: 1,
          borderRadius: '12px',
        }}>
        {sessions?.filter((session: Session) => (
          (selectedInstituition === undefined)
          ||
          (session?.institution?.name === selectedInstituition)
        ))
          .map((
            session: Session,
            index: number) => (
            <ClientSessionCard
              key={index}
              active={false}
              session={session}
              endSession={ () => null}/>
          ))}
      </Stack>
      <Stack
        direction="row"
        alignItems="center"
        justifyContent="center"
        sx={{
          width: '100%',
        }}
      >
        <Pagination
          count={3}
          page={page}
          onChange={handlePageChange}
        />
      </Stack>
=======
      <Stack direction="row" spacing={6} justifyContent="center" alignItems="center" sx={{height: '240px', my: '24px'}}>
        {/* Client Info Paper */}
        <Stack direction="row" spacing={6} justifyContent="center" alignItems="center" sx={{boxShadow: 2, borderRadius: '12px', px: '52px', py: '24px', height: '100%'} }>
          <Avatar src={client?.photoURL?.XL} sx={{ width: '60%', height: '60%' }} />
          <Stack direction="column" spacing={1} justifyContent="center">
            <Typography variant="h3" >
              {client?.fullName}
            </Typography>
            <Typography>{client?.sex}, {client?.dateOfBirth}</Typography>
            <Typography>Phone Number</Typography>
            <Typography sx={{ color: '#007AFF' }}>{client?.phoneNumber}</Typography>
            <Typography>Email</Typography>
            <Typography sx={{color: '#007AFF'}}>{client?.email}</Typography>
          </Stack>
        </Stack>
        {/* Money Info */}
        <Stack direction="column" spacing={4}  justifyContent="center" sx={{height: '100%',}}>
          <Stack direction="row" spacing={4}>
              {/* Average Price per Visit */}
            <Stack
                direction="row"
                alignItems="center"
                justifyContent="start"
                spacing={4}
                sx={{ py: '6px', px: '12px', boxShadow: 2, borderRadius: '12px', backgroundColor: '#F8F8F8' }}
              >
                <AvPayClient /> 
              <Stack direction="column">
                <Typography variant="h4">6000</Typography>
                <Typography>Средняя цена за одно посещение</Typography>
              </Stack>
            </Stack>
              {/* Average Time per Visitation */}
              <Stack direction="row"
            alignItems="center"
            justifyContent="start"
              spacing={4}
                sx={{ py: '4px', px: '12px', boxShadow: 2, borderRadius: '12px', backgroundColor: '#F8F8F8' }}
              >
                <AvTimeClient />
                 <Stack direction="column">
                  <Typography variant="h4">46 мин</Typography>
                  <Typography>Среднее время одного посещения</Typography>
                </Stack>
              </Stack>
              {/* Overall Earnings of all Visitations */}
            </Stack>
            <Stack direction="row"
            alignItems="center"
            justifyContent="start"
              spacing={4}
              sx={{ py: '4px', px: '12px', boxShadow: 2, borderRadius: '12p', backgroundColor: '#F8F8F8', height: '140px'}}
            >
              <AvTimeClient />
               <Stack direction="column">
                <Typography variant="h4">156 000</Typography>
                <Typography>Общая сумма всех посещений</Typography>
              </Stack>
            </Stack>
        </Stack>
      </Stack>
      {/* Active Session - Secion */}
      <Stack direction="column" spacing={4} justifyContent="center" alignItems="center" sx={{ backgroundColor: '#F8F8F8', borderRadius: '12px', my: '24px', py: '12px', width: '100%'}}>
        <Typography variant="h4">Активные сессии</Typography>
        <ClientSessionCard session={session[0]} active={true}
          endSession={() => handleSessioEnd(client?._id, session[0]?._id)} />
      </Stack>
      {/* Histofy Of Sessions - Section */}
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
    </Stack>
  )
}
export default ClientPage;

interface ClientSessionCardProps {
  session: Session;
  active: boolean;
  endSession: () => void;
}

const ClientSessionCard = ({session, active, endSession}: ClientSessionCardProps) => {
  return (
<<<<<<< HEAD
    <Stack
      direction="row"
      justifyContent="space-evenly"
      alignItems="center"
      spacing={4}
      sx={{
        minHeight: '136px',
        width: '100%',
        backgroundColor: '#fff',
        boxShadow: 2,
        borderRadius: '12px',
        px: '10px',
      }}>
      {/* Left Side */}
      <Stack
        direction="row"
        alignItems="center"
        justifyContent="center"
        spacing={8}>
        <Stack
          direction="column"
          spacing={1}>
          {/* Left Part of Session Card */}
          <Typography
            sx={{
              fontWeight: '300',
              fontSize: '16px',
              color: '#979797',
          }}>
            {session?.institution?.location?.address}
          </Typography>
          <Typography
            sx={{
              fontWeight: '500',
              fontSize: '28px',
              color: '#434343',
            }}>
=======
    <Stack direction="row" justifyContent="space-evenly" alignItems="center" sx={{width: '90%', backgroundColor: '#fff', boxShadow: 2, borderRadius: '12px'}}>
      
      {/* Left Side */}
      <Stack direction="row" alignItems="center" justifyContent="space-evenly">
        <Stack direction="column" spacing={2}>
          {/* Left Part of Session Card */}
          <Typography>
            {session?.institution?.address}
          </Typography>
          <Typography>
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
            {session?.institution?.name}
          </Typography>
        </Stack>
        {/* Middle Left Part */}
<<<<<<< HEAD
        <Typography
          sx={{
            fontWeight: '300',
            fontSize: '16px',
            color: '#979797',
        }}
        >{session?.institution?._id}</Typography>
      </Stack>

      {/* Right Side */}
      <Stack
        direction="row"
        alignItems="center"
        justifyContent="center"
        spacing={8}>
        <Stack
          direction="column"
          spacing={2}>
          {/* Left Part of Session Card */}
          <Typography
            sx={{
              fontWeight: '300',
              fontSize: '16px',
              color: '#979797',
            }}>
            {dayjs(dayjs(session?.endTime)).diff(dayjs(session?.startTime), "minutes")} Минут
          </Typography>
          <Typography
            sx={{
              fontWeight: '400',
              fontSize: '24px',
              color: '#434343',
            }}>
            {dayjs(session?.startTime).format('HH:MM')} - {dayjs(session?.endTime).format('HH:MM')}
          </Typography>
        </Stack>
        {/* Middle Left Part */}
        <Typography
          sx={{
            fontWeight: '500',
            fontSize: '28px',
            color: '#434343',
          }}>
          {session?.price} ₸
        </Typography>
=======
        <Typography>{session?.institutionId}</Typography>
      </Stack>

      {/* Right Side */}
      <Stack direction="row" alignItems="center" justifyContent="space-evenly">
        <Stack direction="column" spacing={2}>
          {/* Left Part of Session Card */}
          <Typography>
            {session?.startTime}
          </Typography>
          <Typography>
            {session?.startTime } - {session?.endTime}
          </Typography>
        </Stack>
        {/* Middle Left Part */}
        <Typography>{session?.price}</Typography>
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
      </Stack>
      {active && (
        <Stack direction="row" alignItems="center" justifyContent="space-evenly">
          <Button color="error" variant="outlined" onClick={()=> endSession()}>Завершить</Button>
        </Stack>
      )}
    </Stack>
  )
}