import React from 'react';
import { Box, Button, Typography, Stack, TextField, IconButton } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
<<<<<<< HEAD
import { emitCustomEvent } from 'react-custom-events';

=======
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
import CustomSelect from '../../components/CustomSelect';
import { useMutation } from '@apollo/client';
import { ADD_WORKER } from '../../graphql/mutations/addWorker';
import useLocalStorage from '../../hooks/useLocalStorage';
import { Institution } from '../../interfaces/institution';

interface Props {
  setOpen: () => void;
  institutions: Institution[];
}

const CreateWorker = ({ setOpen, institutions }: Props) => {
  const [store] = useLocalStorage();
<<<<<<< HEAD
  const [selectedInstituition, setSelectedInstitution] = React.useState<string>(institutions[0]?.name);
=======
  const [selectedInstituition, setSelectedInstitution] = React.useState([]);
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
  const [addWorker, { loading, error }] = useMutation(ADD_WORKER, {
    context: {
      headers: {
        Authorization: store.getItem('JWT'),
      }
    },
<<<<<<< HEAD
  });
  const handleAddWorker = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
=======
  })
  const handleAddWorker = (event: React.FormEvent<HTMLFormElement>) => {
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
    const data = new FormData(event.currentTarget);
    const formData = {
      login: data.get('login')?.toString(),
      fullName: data.get('fullName')?.toString(),
      password: data.get('password')?.toString(),
<<<<<<< HEAD
      institutionId: institutions.find(institution => institution.name === selectedInstituition)?._id,
=======
      institutionId: institutions.find(institution => institution.name === selectedInstituition[0])?._id,
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
    }
    console.log(formData)
    addWorker({
      variables: {
        ...formData
      }
    })
<<<<<<< HEAD
  };
  if (loading) emitCustomEvent('loading', loading);
  emitCustomEvent('loading', false);
  if (error) emitCustomEvent('error', error);
=======
  }
  
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
  return (
    <Stack
      component="form"
      direction="column"
      spacing={4}
      justifyContent="center"
      sx={{
        width: '50%',
        borderRadius: '14px',
        boxShadow: 2,
        px: '24px',
        py: '18px',
      }}
      onSubmit={handleAddWorker}
    >
      <Stack direction="row" justifyContent="space-between">
<<<<<<< HEAD
        <Typography
          sx={{
            fontWeight: '600',
            fontSize: '22px',
            my: '6px'
          }}>
          Добавить работника
        </Typography>
        <IconButton
          onClick={() => setOpen()}>
          <CloseIcon />
        </IconButton>
      </Stack>
      <TextField 
        id="fullName"
        name="fullName"
        type="text"
        variant="filled"
        required
        label="Полное имя"
        />
      <TextField 
        id="login"
        name="login"
        type="text"
        variant="filled"
        required
        label="Логин"
        />
      <TextField 
        id="password"
        name="password"
        type="text"
        variant="filled"
        required
        label="Пароль" 
        />
      <CustomSelect
        items={institutions.map(institution => institution.name)}
        selectItems={(item: string) => setSelectedInstitution(item)}
        selectedItem={selectedInstituition} />
      <Button color="primary" variant="contained" type="submit">Добавить</Button>
=======
        <Typography sx={{ fontWeight: '600', fontSize: '16px', my: '6px' }}>Add Worker</Typography>
        <IconButton onClick={() => setOpen()}><CloseIcon /></IconButton>
      </Stack>
      <TextField id="fullName" type="text" variant="filled" required label="Полное имя"/>
      <TextField id="login" type="text" variant="filled" required label="Логин"/>
      <TextField id="password" type="text" variant="filled" required label="Пароль" />
      <CustomSelect
        items={institutions.map(institution => institution.name)}
        selectItems={(items: []) => setSelectedInstitution(items)}
        selectedItems={selectedInstituition} />
      <Button color="primary" variant="contained" type="submit">Add</Button>
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
    </Stack>
  )
}
export default CreateWorker;