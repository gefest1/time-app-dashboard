import * as React from 'react';
<<<<<<< HEAD
import { Box, Button, Typography, Stack, TextField, Checkbox, Divider, Switch, FormControl, InputLabel, InputAdornment, OutlinedInput, Tooltip, IconButton } from '@mui/material';
import TimePicker from 'react-time-picker';
import ArrowBackRoundedIcon from '@mui/icons-material/ArrowBackRounded';
import LinkIcon from '@mui/icons-material/Link';
import DeleteIcon from '@mui/icons-material/Delete';
import PhoneIcon from '@mui/icons-material/Phone';
import { emitCustomEvent } from 'react-custom-events';

import { ReactComponent as TelegramIcon } from '../../images/Specs/telegram.svg';
import { ReactComponent as InstagramIcon } from '../../images/Specs/instagram.svg';
import {ReactComponent as WhatsAppIcon} from '../../images/Specs/whatsapp.svg'
import { ReactComponent as UploadImage } from '../../images/photo_upload.svg';
import { ReactComponent as DescriptionSVG } from '../../images/description.svg';
import { ReactComponent as LocationSVG } from '../../images/location.svg';
import { ReactComponent as VideoUploadSVG } from '../../images/video_upload.svg';
import { ReactComponent as WorkingHoursSVG } from '../../images/working_hours.svg';
import { ReactComponent as CircleActionSVG } from '../../images/Specs/circle_action.svg';
import CustomTimeInput from '../../components/CustomTimeInput';
import axios from 'axios';
import useLocalStorage from '../../hooks/useLocalStorage';
=======
import { styled } from '@mui/material/styles';
import { ListItemText, Box,Button , Typography, Stack, TextField, Checkbox, Divider, Switch } from '@mui/material';
import ArrowBackRoundedIcon from '@mui/icons-material/ArrowBackRounded';
import { ReactComponent as UploadImage } from '../../images/photo_upload.svg';
import { ReactComponent as DescriptionSVG } from '../../images/description.svg';
import {ReactComponent as LocationSVG} from '../../images/location.svg';
import ClubNameInput from '../../components/ClubNameInput';
import CustomTimeInput from '../../components/CustomTimeInput';

>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73

interface Props {
  setOpen: () => void;
}
<<<<<<< HEAD
interface WorkDay {
  day: string;
  time: [string, string];
  rest: [string, string];
}

const addClubQuery = "mutation($avatar: Upload!, $averagePrice: Int!, $city: String!, $description: String!,$address: String!, $email: String!, $gallery: [Upload], $latitude: Float!, $longitude: Float!, $name: String!, $phoneNumber: String!, $type: AllowedInstitutionTypes!){  createInstitution(avatar: $avatar, averagePrice: $averagePrice, city: $city, description: $description,email: $email, gallery: $gallery, latitude: $latitude,longitude:$longitude,name:$name,phoneNumber:$phoneNumber, type: $type, address: $address){_id}}";

export default function CreateClubD({ setOpen }: Props) {
  const [store] = useLocalStorage();

  const [fileSelected, setFileSelected] = React.useState<File>() // also tried <string | Blob>
  const [workHours, setWorkHours] = React.useState<WorkDay[]>(
    [
      {
        day: '',
        time: ['', ''],
        rest: ['', '']
      }
    ]
  )
  const handleImageChange = function (e: React.ChangeEvent<HTMLInputElement>) {
    const fileList = e.target.files;

    if (!fileList) return;
      
    setFileSelected(fileList[0]);
  }

  const address = {
    city: "",
    latitude: 0,
    longitude: 0,
    street: "",
  }
  const getAddress = async (street: string) => {
    const query = encodeURI(street)
    const key = encodeURI('AIzaSyAGI5Zt0sx03csbLyjsJs4KV9HgaUGlUM0');
    // @ts-ignore
    const { data } = await axios.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${query}&key=${key}`)
      .catch(error => console.log(error))
    const { results } = data;
    address.city = results[0].address_components[3].long_name;
    address.longitude = parseInt(results[0].geometry.location.lng);
    address.latitude = parseInt(results[0].geometry.location.lat);
    address.street = results[0].formatted_address;
  }
  const handleAddClub = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const reqData = new FormData();
    const data = new FormData(event.currentTarget)
    getAddress(data.get('address')?.toString() || "");
    const formData = {
      avatar: null,
      averagePrice: parseInt(data.get('price')?.toString() || "0"),
      city: address.city || "",
      description: data.get('description')?.toString() || "",
      email: data.get('email')?.toString() || "",
      gallery: [null, null],
      address: address.street,
      latitude: address.latitude || 0,
      longitude: address.longitude || 0,
      name: data.get('club-name')?.toString() || "",
      phoneNumber: data.get('phone')?.toString() || "",
      type: "Fitness"
    };
    const map = {
      avatar: ['variables.avatar'],
    };
    const operations = {
      query: addClubQuery,
      variables: formData
    }
    reqData.append('operations', JSON.stringify(operations));
    reqData.append('map', JSON.stringify(map));
    reqData.append('avatar', fileSelected || "");

    axios.post('http://167.172.162.8:3001/graphql', reqData, {
      headers: {
        'Content-Type': 'multipart/form-data',
        'Authorization': store.getItem('JWT') || "",
        'Access-Control-Allow-Origin': '*',
      },
    }
    ).then(response => {
      emitCustomEvent('create-club-success', response);
    }).catch(error => {
      emitCustomEvent('create-club-error', error);
    });
  };
  return (
    <Stack
      component="form"
      onSubmit={handleAddClub}
      direction="column"
      alignItems="flex-start"
      justifyContent="flex-start"
      spacing={2}
      sx={{
        width: '80%',
      }}>
=======
const Input = styled('input')({
  display: 'none',
});

export default function CreateClubD({ setOpen }: Props) {

  return (
    <Box sx={{
      height: '100vh',
      width: '80%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',
      justifyContent: 'flex-start',
    }}>
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
      {/* Header */}
      <Stack
        direction="row"
        justifyContent="space-between"
        sx={{
          boxShadow: 2,
          borderRadius: "14px",
          width: '100%',
          py: '8px',
          px: '28px',
        }}>
        <Box sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'flex-start',
          justifyContent: 'center',
        }}>
          <Button
            color="primary"
            variant="text"
            startIcon={<ArrowBackRoundedIcon />}
            onClick={() => setOpen()}
<<<<<<< HEAD
          >Назад</Button>
=======
            >Назад</Button>
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
          <Typography sx={{
            fontWeight: '600',
            fontSize: '22px',
          }}>Добавление нового Объекта</Typography>
        </Box>
        <Box sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'flex-start',
          justifyContent: 'center',
        }}>
          <Button sx={{
            borderRadius: '14px',
            height: '48px',
            width: '140px',
            fontSize: '14px',
            fontWeight: '600',
<<<<<<< HEAD
          }} color="primary" variant="contained" type="submit">Сохранить</Button>
        </Box>
      </Stack>
      {/* Name Section */}
      <input
        style={{
        width: '640px',
        height: '56px',
        fontSize: '1.75rem',
        backgroundColor: 'transparent',
        borderRadius: '6px',
        color: '#979797',
        margin: '24px 0px',
        border: 'none',
        outline: 'none'
        }}
        required
        name="club-name"
        placeholder="Введите название объекта"
        autoFocus
      />
=======
          }} color="primary" variant="contained">Сохранить</Button>
        </Box>
      </Stack>
      {/* Name Section */}
      <ClubNameInput />
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
      {/* Photo Section */}
      <Stack
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        sx={{
          boxShadow: 2,
          borderRadius: "14px",
          width: '100%',
          height: '480px',
          py: '12px',
          px: '24px',
        }}>
<<<<<<< HEAD
        {!fileSelected ? (
          <UploadImage />
        ) : (
            <>
            <img alt="not-found"
              style={{
                borderRadius: '15px',
                width: '493px',
              height: '432px',}}
                src={URL.createObjectURL(fileSelected)} />
              <Tooltip title="Delete" placement="bottom-end">
                <IconButton onClick={() => setFileSelected(undefined)}>
                  <DeleteIcon />
                </IconButton>
              </Tooltip>
              </>
              )
        }
        <Stack spacing={2}>
=======
        <UploadImage />
        <Box>
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
          <Typography sx={{
            fontWeight: '600',
            fontSize: '28px',
          }}>Фотографий пока нет</Typography>
          <Typography sx={{
            fontSize: '20px',
            my: '2px',
          }}>Но вы можете добавить</Typography>
<<<<<<< HEAD
          <label htmlFor="upload-image"
            style={{
              width: '60%', 
              backgroundColor: '#007AFF',
              height: '42px',
              borderRadius: '14px',
              fontSize: '18px',
              color: '#fff',
              padding: '10px 10px',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
          }}
          >Добавить Фото</label>
          <input
            required
            id="upload-image"
            accept="image/*"
            type="file"
            hidden
            onChange={handleImageChange}
            />
        </Stack>
      </Stack>

      {/* Price Section */}
      <Stack
        direction="row"
        alignItems="center"
        justifyContent="center"
        sx={{
          boxShadow: 2,
          borderRadius: "14px",
          width: '100%',
          height: '120px',
          py: '12px',
          px: '24px',
        }}>
        <Stack sx={{
          width: '100%'
        }}>
        <Typography
            sx={{
            fontWeight: '600',
            fontSize: '28px',
            my: '6px',
            }}>
            Цена
          </Typography>
         <FormControl 
            fullWidth
            sx={{
              m: 1
            }}>
            <InputLabel
              htmlFor="outlined-adornment-amount"
            >Цена
            </InputLabel>
            <OutlinedInput
              name="price"
              type="number"
              required
              id="outlined-adornment-amount"
              startAdornment={
                <InputAdornment
                  position="start">
                  ₸
                </InputAdornment>
              }
            label="Amount"
          />
          </FormControl>
          </Stack>
      </Stack>
=======
          <label htmlFor="upload-image">
            <Input id="upload-image" accept="image/*" type="file" />
            <Button variant="contained" color="primary" sx={{
              width: '60%',
              height: '42px',
              my: '16px',
              borderRadius: '14px',
              fontSize: '14px',
            }}>
              Добавить Фото
            </Button>
          </label>
        </Box>
      </Stack>

      {/* Price Section */}
      <Stack></Stack>
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
      
      {/* Description Section */}
      <Stack
        direction="row"
        alignItems="center"
        spacing={4}
        sx={{
          my: '20px',
          boxShadow: 2,
          borderRadius: "14px",
          width: '100%',
          height: '320px',
          py: '18px',
          px: '24px',
        }}>
<<<<<<< HEAD
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            width: '100%'
          }}>
          <Typography
            sx={{
            fontWeight: '600',
            fontSize: '28px',
            my: '6px',
            }}>
            Описание
          </Typography>
          <TextField
            required
            name="description"
            multiline
            rows={8}
            sx={{
              width: '100%',
              outline: 'none'
            }} />
        </Box>
        <Box sx={{}}>
          <DescriptionSVG />
=======
        <Box sx={{display: 'flex', flexDirection: 'column', width: '100%'}}>
          <Typography sx={{
            fontWeight: '600',
            fontSize: '28px',
            my: '6px',
          }}>Описание</Typography>
          <TextField multiline rows={ 8} sx={{width: '100%', outline: 'none'}}/>
        </Box>
        <Box sx={{}}>
          <DescriptionSVG/>
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
        </Box>
      </Stack>
      
      {/* Address */}
      <Stack
        direction="row"
        alignItems="center"
        spacing={4}
        sx={{
          my: '20px',
          boxShadow: 2,
          borderRadius: "14px",
          width: '100%',
<<<<<<< HEAD
          height: '120px',
          py: '18px',
          px: '24px',
        }}>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            width: '100%'
          }}>
          <Typography
            sx={{
            fontWeight: '600',
            fontSize: '28px',
            my: '6px',
            }}>
            Адрес
          </Typography>
          <TextField
            required
            variant="standard"
            placeholder="Указать адрес"
            name="address" />
        </Box>
        <LocationSVG />
      </Stack>
      
      {/* Working hours */}
          {/* <WorkingHours/> */}
      {/* Video */}
      <Stack
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        spacing={2}
        sx={{
          boxShadow: 2,
          borderRadius: "14px",
          width: '100%',
          minHeight: '120px',
          py: '12px',
          px: '24px',
        }}>
        <Stack
          sx={{
          width: '90%',
        }}>
          <Typography
            sx={{
            fontWeight: '600',
            fontSize: '28px',
            my: '6px',
            }}>
            Видео
          </Typography>
          <FormControl
            fullWidth
            sx={{ m: 1, pt: 1 }}>
          <OutlinedInput
            id="outlined-adornment-amount"
              startAdornment={
                <InputAdornment
                  position="start">
                  <LinkIcon
                    sx={{
                      height: '20px',
                      width: '20px',
                      transform: 'rotate(45deg)'
                    }} />
                </InputAdornment>
              }
            label="Amount"
              sx={{
                height: '65px',
              }}
            placeholder="Ссылка на ролик с YouTube"
          />
          </FormControl>
        </Stack>
        <VideoUploadSVG />
      </Stack>

      {/* Certifocates */}
      {/* <Stack></Stack> */}

      {/* Additional */}
      {/* <AdditionalClubInfo /> */}

      {/* Contacts */}
      <Stack direction="column" spacing={4} justifyContent="center" alignItems="center" sx={{ width: '100%' }}>
        <Stack direction="row" spacing={4} justifyContent="center" alignItems="center" sx={{ width: '100%' }}>
          <ContactCard
            disabled={false}
            name="phone"
            icon={<PhoneIcon sx={{width: '60px', height: '60px'}}/>}
            type="phone" />
          <ContactCard
          disabled={true}
            name="instagram"
            icon={<InstagramIcon/>}
            type="instagram"  
            />
          </Stack>
        <Stack direction="row" spacing={4} justifyContent="center" alignItems="center" sx={{ width: '100%' }}>
          <ContactCard
          disabled={true}
            name="whatsapp"
            icon={<WhatsAppIcon/>}
            type="whatsapp"  
            />
          <ContactCard
          disabled={true}
            name="telegram"  
            icon={<TelegramIcon />}
            type="telegram"  
            />
        </Stack>
      </Stack>
    </Stack>
  );
};
interface CantactCardProps {
  icon: React.ReactElement;
  name:'phone' | 'instagram' | 'whatsapp' | 'telegram';
  type: 'phone' | 'instagram' | 'whatsapp' | 'telegram';
  disabled: boolean;
}

const ContactCard = ({ icon, type, name, disabled }: CantactCardProps) => {
  const inputPrefex = [
    {
      type: 'phone',
      header: 'Добавить номер телефона',
      text: 'Номер телефона',
      prefex: '+7',
    },
    {
      type: 'instagram',
      header: 'Добавить профиль в Instagram',
      text: 'Intsagram',
      prefex: 'instagram.com/',
    },
    {
      type: 'whatsapp',
      header: 'Добавить ноемр WhatsApp',
      text: 'WhatsApp',
      prefex: 'wa.me/',
    },
    {
      type: 'telegram',
      header: 'Добавить Telegram',
      text: 'Telegram',
      prefex: 'tg.me/',
    },
  ];
  const items = inputPrefex.find(input => input.type === type);
  return (
    <Stack
      direction="column"
      alignItems="center"
      justifyContent="center"
      sx={{
        width: '100%',
        height: '200px',
        boxShadow: 2,
        borderRadius: '6px',
        p: '10px'
      }}
    >
      <Stack
        direction="column"
        alignItems="flex-start"
        justifyContent="flex-start"
        spacing={2}
        sx={{ width: '100%' }}
      >
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="center"
          spacing={2}
        >
          <Box sx={{
            width: '60px',
            height: '60px',
          }}>
            {icon}
          </Box>
          <Typography variant="h5">{items?.header}</Typography>
        </Stack>
        <Stack direction="row" spacing={2} justifyContent="space-between" alignItems="center" sx={{ width: '100%' }}>
          <FormControl fullWidth >
            <InputLabel htmlFor="outlined-adornment-amount">{items?.text}</InputLabel>
            <OutlinedInput
              id="outlined-adornment-amount"
              startAdornment={<InputAdornment position="start">{items?.prefex}</InputAdornment>}
              label="Amount"
              name={name}
              disabled={disabled}
            />
          </FormControl>
          <IconButton sx={{ width: '60px', height: '60px', backgroundColor: '#CCCCCC', color: '#fff' }}>
            <CircleActionSVG />
          </IconButton>
        </Stack>
      </Stack>
    </Stack>
  )
};

interface WorkingHoursProps {
  setWorkHours: (arg0: any) => void;
  workHours: any[];
}
const WorkingHours = ({setWorkHours, workHours}: WorkingHoursProps ) => {
  return (
    <Stack
      direction="column"
      alignItems="start"
      spacing={4}
      sx={{
        my: '20px',
        boxShadow: 2,
        borderRadius: "14px",
        width: '100%',
        height: '320px',
        py: '18px',
        px: '24px',
      }}>
      <Typography
        sx={{
          fontWeight: '600',
          fontSize: '28px',
          my: '6px',
        }}>
        График Работы
      </Typography>
      <Stack
        direction="row"
        spacing={2}
        justifyContent="space-between"
        alignItems="center"
        sx={{
          width: '100%'
        }}>
        {/* Right Side */}
        <Stack
          direction="row"
          spacing={2}
          justifyContent="center"
          alignItems="center">
          {/* WeekDays */}
          <Stack
            direction="row"
            spacing={2}
            justifyContent="center"
            alignItems="center">
            <Checkbox />
            <Typography
              sx={{
                fontWeight: '600',
                fontSize: '16px'
              }}>
              Все будни
            </Typography>
            <TimePicker
              disableClock
              format="hh:mm"
              value={workHours[0]?.time[0]}
              onChange={(newValue) => {
                setWorkHours((prevState:any) => ({
                  ...prevState,
                  time: newValue
                })
                )
              }}
            />
            <Typography>-</Typography>
            <CustomTimeInput time="22:00" />
          </Stack>
          {/* Weekends */}
          <Stack
            direction="row"
            spacing={2}
            justifyContent="center"
            alignItems="center">
            <Checkbox />
            <Typography sx={{
              fontWeight: '600',
              fontSize: '16px'
            }}>
              Все будни
            </Typography>
            <CustomTimeInput time="09:00" />
            <Typography>-</Typography>
            <CustomTimeInput time="22:00" />
          </Stack>
        </Stack>
        {/* Left Side */}
        <WorkingHoursSVG />
      </Stack>
    </Stack>
  );
};

const AdditionalClubInfo = () => {
  return (
    <Stack
      direction="column"
      alignItems="start"
      spacing={4}
      sx={{
        my: '20px',
        boxShadow: 2,
        borderRadius: "14px",
        width: '100%',
        py: '18px',
        px: '24px',
      }}>
      <Typography
        sx={{
          fontWeight: '600',
          fontSize: '16px'
        }}>
        Дополнительно
      </Typography>
      <Stack
        direction="column"
        divider={
          <Divider
            orientation="horizontal" />
        }
        spacing={2}
        sx={{
          width: '100%'
        }}
      >
        <Stack
          direction="row"
          spacing={2}
          justifyContent="space-between"
          alignItems="center"
          sx={{
            width: '100%'
          }}>
          <Typography
            sx={{
              fontWeight: '400',
              fontSize: '14px'
            }}>
            Оплата картой
          </Typography>
          <Switch />
        </Stack>
        <Stack
          direction="row"
          spacing={2}
          justifyContent="space-between"
          alignItems="center"
          sx={{
            width: '100%'
          }}>
          <Typography
            sx={{
              fontWeight: '400',
              fontSize: '14px'
            }}>
            WiFi
          </Typography>
          <Switch />
        </Stack>
        <Stack
          direction="row"
          spacing={2}
          justifyContent="space-between"
          alignItems="center"
          sx={{
            width: '100%'
          }}>
          <Typography
            sx={{
              fontWeight: '400',
              fontSize: '14px'
            }}>
            Платная Парковка
          </Typography>
          <Switch />
        </Stack>
        <Stack
          direction="row"
          spacing={2}
          justifyContent="space-between"
          alignItems="center"
          sx={{
            width: '100%'
          }}>
          <Typography
            sx={{
              fontWeight: '400',
              fontSize: '14px'
            }}>
            Бесплатная Паркока
          </Typography>
          <Switch />
        </Stack>
        <Stack
          direction="row"
          spacing={2}
          justifyContent="space-between"
          alignItems="center"
          sx={{
            width: '100%'
          }}>
          <Typography sx={{ fontWeight: '400', fontSize: '14px' }}>
            Бассейн
          </Typography>
          <Switch />
        </Stack>
        <Stack direction="row" spacing={2} justifyContent="space-between" alignItems="center" sx={{ width: '100%' }}>
          <Typography sx={{ fontWeight: '400', fontSize: '14px' }}>
            Русская Баня
          </Typography>
          <Switch />
        </Stack>
        <Stack direction="row" spacing={2} justifyContent="space-between" alignItems="center" sx={{ width: '100%' }}>
          <Typography sx={{ fontWeight: '400', fontSize: '14px' }}>
            Хамам
          </Typography>
          <Switch />
        </Stack>
      </Stack>

    </Stack>
=======
          height: '320px',
          py: '18px',
          px: '24px',
        }}>
        <Box sx={{ display: 'flex', flexDirection: 'column', width: '100%' }}>
          <Typography sx={{
            fontWeight: '600',
            fontSize: '28px',
            my: '6px',
          }}>Адрес</Typography>
          <TextField variant="standard" placeholder="Указать адрес" />
          </Box>
          <LocationSVG />
      </Stack>
      
      {/* Working hours */}
      <Stack
        direction="column"
        alignItems="start"
        spacing={4}
        sx={{
          my: '20px',
          boxShadow: 2,
          borderRadius: "14px",
          width: '100%',
          height: '320px',
          py: '18px',
          px: '24px',
        }}>
          <Typography sx={{
            fontWeight: '600',
            fontSize: '28px',
            my: '6px',
        }}>График Работы</Typography>
        <Box sx={{ display: 'flex', flexDirection: 'row' }}>
          <Stack direction="column" spacing={2}>
            <Stack direction="row" spacing={2}>
              <Checkbox />
              <Typography sx={{ fontWeight: '600', fontSize: '16px' }}>Все будни</Typography>
              <CustomTimeInput time="09:00"/>
              <Typography>-</Typography>
              <CustomTimeInput time="22:00" />
            </Stack>
          </Stack>
        </Box>
      </Stack>

      {/* Video */}
      <Stack></Stack>

      {/* Certifocates */}
      <Stack></Stack>

      {/* Additional */}
      <Stack
        direction="column"
        alignItems="start"
        spacing={4}
        sx={{
          my: '20px',
          boxShadow: 2,
          borderRadius: "14px",
          width: '100%',
          py: '18px',
          px: '24px',
        }}>
        <Typography sx={{ fontWeight: '600', fontSize: '16px' }}>Дополнительно</Typography>
        <Stack
          direction="column"
          divider={<Divider orientation="horizontal" />}
          spacing={2}
          sx={{width: '100%'}}
        >
          <Stack direction="row" spacing={2} justifyContent="space-between" alignItems="center" sx={{width: '100%'}}>
            <Typography sx={{ fontWeight: '400', fontSize: '14px' }}>
              Оплата картой
            </Typography>
            <Switch />
          </Stack>
          <Stack direction="row" spacing={2} justifyContent="space-between" alignItems="center" sx={{width: '100%'}}>
            <Typography sx={{ fontWeight: '400', fontSize: '14px' }}>
              WiFi
            </Typography>
            <Switch />
          </Stack>
          <Stack direction="row" spacing={2} justifyContent="space-between" alignItems="center" sx={{width: '100%'}}>
            <Typography sx={{ fontWeight: '400', fontSize: '14px' }}>
              Платная Парковка
            </Typography>
            <Switch />
          </Stack>
          <Stack direction="row" spacing={2} justifyContent="space-between" alignItems="center" sx={{width: '100%'}}>
            <Typography sx={{ fontWeight: '400', fontSize: '14px' }}>
              Бесплатная Паркока
            </Typography>
            <Switch />
          </Stack>
          <Stack direction="row" spacing={2} justifyContent="space-between" alignItems="center" sx={{width: '100%'}}>
            <Typography sx={{ fontWeight: '400', fontSize: '14px' }}>
              Бассейн
            </Typography>
            <Switch />
          </Stack>
          <Stack direction="row" spacing={2} justifyContent="space-between" alignItems="center" sx={{width: '100%'}}>
            <Typography sx={{ fontWeight: '400', fontSize: '14px' }}>
              Русская Баня
            </Typography>
            <Switch />
          </Stack>
          <Stack direction="row" spacing={2} justifyContent="space-between" alignItems="center" sx={{width: '100%'}}>
            <Typography sx={{ fontWeight: '400', fontSize: '14px' }}>
              Хамам
            </Typography>
            <Switch />
          </Stack>
        </Stack>

      </Stack>

      {/* Contacts */}
      <Stack></Stack>
    </Box>
>>>>>>> 51c81a50b0ee3eb5149f26dc5b45d10da534fa73
  );
};