import React from 'react'
import { Stack, Box, Typography, Grid, Divider } from '@mui/material';
import dayjs from 'dayjs';
import SessionCard from '../components/SessionCard';
import CustomSelect from '../components/CustomSelect';
import { useQuery, useLazyQuery } from '@apollo/client';
import { emitCustomEvent } from 'react-custom-events';
import { Session } from '../interfaces/session';
import { Institution } from '../interfaces/institution';
import { GET_STATS } from '../graphql/query/getStats';
import { VISITATIONS_OF_CLIENT } from '../graphql/query/visitationsOfClient';
import { GET_HISTORY } from '../graphql/query/getHistory';
import useLocalStorage from '../hooks/useLocalStorage';
import { useInstitution } from '../utils/recoil/instituition.hook';
import { getStatsByInterval } from '../helpers/getStatsByInterval';
import { ReactComponent as AvPayClient } from '../images/av_pay_client.svg';
import { ReactComponent as AvTimeClient } from '../images/av_time_client.svg';
type Interval = 'day' | 'month' | 'year' | 'date'
type AllowedStatsSeparationType = "Day" | "Month" | "Year"
interface Stat {
  earnings: number
  intervalType: AllowedStatsSeparationType
  intervalValue: number
  numOfSessions: number
}


const HistoryPage = () => {
  const [store] = useLocalStorage();
  const [institutions, { setInstitution }] = useInstitution();
  
  const [openClient, setOpenClient] = React.useState<boolean>(false);
  const [client, setClient] = React.useState()
  const [selectedInstituition, setSelectedInstitution] = React.useState<Institution>();
  const [stats, setStats] = React.useState<Stat[]>();
  const [interval, setInterval] = React.useState<Interval>("month");
  const [separateBy, setSeparateBy] = React.useState<AllowedStatsSeparationType>("Month");
  const [dates, setDates] = React.useState({
    firstDate: dayjs().set('month', dayjs().get('months') - 7).toISOString(),
    secondDate: dayjs().toISOString()
  });
  React.useEffect(() => {
    if (interval === "day") {
      setDates(prevState => ({
        ...prevState,
        firstDate: dayjs().set("day", dayjs().get("day") - 7).toISOString()
      }))
      setSeparateBy("Day");
    } else if (interval === "date") {
      setDates(prevState => ({
        ...prevState,
        firstDate: dayjs().set('date', dayjs().get('date') - 31).toISOString()
      }))
      setSeparateBy("Day");
    } else if (interval === "month") {
      setDates(prevState => ({
        ...prevState,
        firstDate: dayjs().set("month", dayjs().get("month") - 4).toISOString()
      }))
      setSeparateBy("Month");
    } else  if (interval === "year") {
      setDates(prevState => ({
        ...prevState,
        firstDate: dayjs().set("year", dayjs().get("year") - 2).toISOString()
      }))
      setSeparateBy("Year");
    }
  }, [interval])

  const {
    data: visitationsData,
    loading: visitationsLoading,
    error: visitationsError
  } = useQuery(VISITATIONS_OF_CLIENT, {
    context: {
      headers: {
        Authorization: store?.getItem('JWT'),
      },
    },
    variables: {
      "page": 1,
      "sortBy": 'Date',
      "isActive": false,
    },
    onCompleted: (data) => {
      getHistory();
    },
  });

  const [getHistory, {
    loading: historyLoading,
    error: historyError,
  }] = useLazyQuery(GET_STATS, {
    context: {
      headers: {
        Authorization: store?.getItem('JWT'),
      },
    },
    variables: {
      firstDate: dates?.firstDate,
      secondDate: dates?.secondDate,
      "separateBy": separateBy,
      "institutionIds": institutions?.map(institution => institution?._id),
    },
    onCompleted: (data) => {
      setStats(data.getStatsByDates);
    }
  });

  if (visitationsLoading || historyLoading) emitCustomEvent('loading', visitationsLoading || historyLoading);
  emitCustomEvent('loading', false);
  if (visitationsError || historyError) emitCustomEvent('error', visitationsError || historyError)

  const sessions = visitationsData?.getVisitationsForAdminPanel;

  const handleOpenClient = (index: number) => {
    setClient(sessions[index]?.client);
    setOpenClient(true);
  };
  const handleIntervalChange = (interval: Interval): void => {
    setInterval(interval)
  };

  const handleIntervalStyles = (type: string) => {
    return interval === type ? {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      height: '32px',
      width: '23%',
      borderRadius: '6px',
      py: '15px',
      backgroundColor: '#f7f7f7',
      boxShadow: 1,
    } : {}
  }

  return (
    <Stack
      direction="column"
      spacing={4}>
      {/* Header Section */}
      <Stack
        direction="row"
        justifyContent="space-between"
        alignItems="center">
        <Typography
          variant="h4"
          sx={{
            color: 'black',
          }}>
          Статистика
        </Typography>
        <Stack
          direction="row"
          justifyContent="center"
          alignItems="center"
          spacing={6}>
        {/* Switch Dropdown */}
          <CustomSelect
            items={institutions?.map(institution => institution?.name)}
            selectItems={(item: Institution) => setSelectedInstitution(item)}
            selectedItem={selectedInstituition?.name}
          />
          {/* Date Interval Selection */}
          <Stack
            direction="row"
            alignItems="center"
            justifyContent="space-evenly"
            spacing={4}
            sx={{
              width: '400px',
              height: '52px',
              py: "5px",
              px: '10px',
              borderRadius: '6px',
              backgroundColor: '#fff',
              color: '#007AFF',
              p: '10px',
            }}>
            <Box
              sx={handleIntervalStyles('year')}
              onClick={() => handleIntervalChange('year')}
            >
              Год
            </Box>
            <Box
              sx={handleIntervalStyles("month")}
              onClick={()=> handleIntervalChange('month')}
            >
              Месяц
            </Box>
            <Box
              sx={handleIntervalStyles("day")}
              onClick={()=> handleIntervalChange('day')}
            >
              Неделя
            </Box>
            <Box
              sx={handleIntervalStyles("date")}
              onClick={()=> handleIntervalChange('date')}
            >
              День
            </Box>
          </Stack>
        </Stack>
      </Stack>

      {/* Stats Boxes Section*/}   
      <Stack
          direction="row"
          justifyContent="center"
          alignItems="center"
          spacing={4}
          >
          {/* Stats Container */}
          <Stack
          direction="row"
          justifyContent="center"
          alignItems="center"
          spacing={4}
          sx={{
            width: '100%',
          }}
          >
            <Stack
              direction="column"
              alignItems="start"
              justifyContent="start"
              sx={{
                width: "100%",
                height: '250px',
                backgroundColor: '#fff',
                p: '10px',
              }}
            >
              <Typography
                variant="h6">
                Октябрь
              </Typography>
              <Stack
                direction="row"
                alignItems="flex-end"
                justifyContent="flex-start"
                spacing={2}
                sx={{
                  width: "100%",
                  height: '250px',
                  px: '10px',
                }}
              >
                {stats?.map((
                  stat: Stat,
                  index: number,
                  self: Stat[]
                ) => (
                  <StatBox
                    earnings={stat?.earnings}
                    all={self?.length}
                    people={stat?.numOfSessions}
                    last={index === self?.length - 1}
                  />
                ))}
              </Stack>
            </Stack>
          </Stack>
          <Stack
            direction="column"
            justifyContent="center"
            alignItems="center"
            spacing={2}
          >
             <Stack
              direction="row"
              alignItems="center"
              justifyContent="start"
              spacing={4}
              sx={{
                width: '200px',
                py: '20px',
                px: '20px',
                boxShadow: 2,
                borderRadius: '12px',
                backgroundColor: '#F8F8F8'
              }}
              >
              <Stack
                direction="column">
                <Typography
                  variant="h4">
                  {Math.floor(
                    stats?.map(
                      stat => stat.earnings
                    )?.reduce(
                      (acc, stat) => acc + stat
                    ) || 0
                  ).toFixed(2)} ₸
                </Typography>
                <Typography>
                  Общая сумма всех посещений
                </Typography>
              </Stack>
            </Stack>
            <Stack
              direction="row"
              alignItems="center"
              justifyContent="start"
              spacing={4}
              sx={{
                width: '200px',
                py: '20px',
                px: '20px',
                boxShadow: 2,
                borderRadius: '12px',
                backgroundColor: '#F8F8F8'
              }}>
              <Stack
                direction="column"
              >
                <Typography
                  variant="h4">
                  {
                      stats?.flatMap(
                        stat => stat?.numOfSessions
                      )?.reduce((acc, stat ) => acc + stat) || 0
                  }
                </Typography>
                <Typography>
                  Количество посещений
                </Typography>
                </Stack>
            </Stack>
          </Stack>
      </Stack>
      
      {/* SubHeader Section */}
      <Stack
        direction="row"
        justifyContent="space-between"
        alignItems="center">
        <Typography
          variant="h4"
          sx={{
            color: 'black',
          }}>
          История Посещений
        </Typography>
        {/* Switch Dropdown */}
      </Stack>

      <Divider />
      {/* History Cards */}
      <Stack
        direction="column"
        sx={{
          overflow: 'scroll',
          maxHeight: '600px',
          boxShadow: 1,
          borderRadius: '12px',
        }}>
        {sessions?.filter((session: Session) => (
          (selectedInstituition === undefined)
          ||
          (session?.institution?.name === selectedInstituition)
        ))
          .map((
            session: Session,
            index: number) => (
            <SessionCard
              key={index}
              session={session}
              setOpen={
                () => handleOpenClient(index)
              } />
          ))}
      </Stack>
    </Stack>
  )
}

export default HistoryPage

interface StatBoxProps {
  earnings: number;
  people: number;
  all: number;
  last: boolean;
}
const StatBox = ({ earnings,
  people,
  all,
  last }: StatBoxProps) => {
  const width = 100 / all;
  const scale = earnings / 10;

  return (
    <Stack
      direction="column"
      alignItems="center"
      justifyContent="center"
      sx={{
        width: `${width}%`,
      }}
      >
      <Box sx={{
        backgroundColor: last ? '#62B4FF' : '#CCE4FF',
        height: `${scale * 1}px`,
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: '6px',
      }}
      >
      <Typography>
        {earnings} ₸
      </Typography>
      <Typography>
        {people} чел.
      </Typography>
    </Box>
  </Stack>
  )
}
