import { gql } from '@apollo/client';

export const LOGIN = gql`
  query(
    $login: String!
    $password: String!
  ){
    loginAsOwner(
      login: $login
      password: $password
      ){
        _id
        token
        email
        phoneNumber
        
        fullName
      }
  }
`;