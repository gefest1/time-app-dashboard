import { gql } from '@apollo/client';

export const GET_ALL_CHATS = gql`
  query($page: Int!) {
    getChatsOfAUser(page: $page){
      _id
      lastMessage {
        text
        dateSent
      }
      user {
        __typename
        ...on Client {
          _id
          fullName
          photoURL {
            M
            XL
            }
          },
        __typename
        ...on Worker {
          _id
          fullName
        }
        __typename 
        ...on Owner {
          _id
          fullName
          }
      }
    }
  }
`;