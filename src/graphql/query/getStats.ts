import { gql } from '@apollo/client';

export const GET_STATS = gql`
  query(
    $firstDate: DateTime!
    $institutionIds: [String!]!
    $secondDate: DateTime!
    $separateBy: AllowedStatsSeparationType!
  ) {
    getStatsByDates(
      firstDate: $firstDate
      institutionIds: $institutionIds
      secondDate: $secondDate
      separateBy: $separateBy
    ) {
    earnings
      intervalType
      intervalValue
      numOfSessions
    }
  }
`;