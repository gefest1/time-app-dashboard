import { gql } from '@apollo/client';

export const GET_HISTORY = gql`
  query(
    $firstDate: DateTime!
    $institutionId: String!
    $secondDate: DateTime!
  ) {
    getVisitationsByPeriodsOfTime(
      firstDate: $firstDate
      institutionId: $institutionId
      secondDate: $secondDate
    ) {
      _id
      endTime
      startTime
      client {
        _id
        email
        fullName
      }
      price
      
    }
  }
`;
