import { gql } from '@apollo/client';

export const GET_INSTITUTION = gql`
  query{
  getInstitutionsOfOwner{
    _id
    name
    avatarURL {
      M
      XL
      thumbnail
    }
    city
    averagePrice
    # location {
    #   address
    #   latitude 
    #   longitude
    # }
    contacts{
      email
      instagram
      whatsApp
      phoneNumber
    }
    numOfWorkers
    workers {
      fullName
    }
    rating
    ratingCount
  }
}
`;