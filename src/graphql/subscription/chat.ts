import { gql } from '@apollo/client';
export const CHAT = gql`

subscription($chatId: String!) {
  viewChat(chatId: $chatId) {
    user {
      __typename
      ... on Client {
        _id
      }
      __typename
      ... on Worker {
        _id
      }
      __typename
      ... on Owner {
        _id
      }
    }
    text
    dateSent
    isViewed
  }
}
`;