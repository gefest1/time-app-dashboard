import { getTime } from './getTime';

export const getTimeDiff = (startDate: string, endDate: string) => {
  const startTime = new Date(startDate).getTime();
  const endTime = new Date(endDate).getTime();
  const dateDiff = endTime - startTime;
  return getTime(dateDiff);
};